/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.	If not, see <https://www.gnu.org/licenses/>.
 */

/* FUSION SHELL (FSH) - A BASH CLONE FOR FUSION */

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <textmode.h>
#include <path.h>
#include <vfs.h>
#include <elf32.h>

int main() {
  text_clear();
  puts("fsh (Fusion Shell) release 0.1a\nCopyright <C> 2020 Succboy6000 (Thanh Vinh Nguyen)\nThis program comes with ABSOLUTELY NO WARRANTY.\nThis is free software, and you are welcome to redistribute it under certain conditions.\n");
  while(1) {
    printf("%s # ", path_cwd);
    char cmdbuf[4096];
    gets(cmdbuf);
		if(!strlen(cmdbuf)) continue; // nothing was typed
    /* chop command up into args */
    uint32_t argc = 1, cmdbuf_len = strlen(cmdbuf);
    char **argv = malloc(sizeof(char*));
    *argv = cmdbuf;
    for(uint32_t i = 0; i < cmdbuf_len; i++) {
      if(cmdbuf[i] == ' ') {
        argv = realloc(argv, ++argc * sizeof(char*));
        argv[argc - 1] = &cmdbuf[i + 1];
        cmdbuf[i] = '\0';
      }
    }
    /* search binaries */
    char fname[1024];
    sprintf(fname, "/bin/%s.elf", argv[0]);
		vfs_node_t *file = path_getnode(fname);
		if(file == NULL) {
			printf("%s: command not found\n", argv[0]);
			continue;
		}
		vfs_open(file, 0);
		uint8_t *t = malloc(file->len);
		vfs_read(file, 0, file->len, t);
    uintptr_t addr = 0;
		void (*ep)(int, char**) = (void*) elf32_load((void*) t, file->len, ELF32_MODE_LOAD_BINS, &addr);
		vfs_close(file);
		free(t);
		(*ep)(argc, argv);
    free((void*) addr);
  }
	return 0;
}
