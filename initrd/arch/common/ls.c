/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.	If not, see <https://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <path.h>
#include <string.h>
#include <stdlib.h>
#include <vfs.h>

static void ls_primitive(void) {
  vfs_node_t *node = path_getnode(path_cwd);
  if(node == NULL) return;
  if(node->flags == VFS_TYPE_DIR) {
    /* list directory */
    for(uint32_t i = 0; ; i++) {
  		struct dirent *de = vfs_readdir(node, i);
  		if(de == NULL) break;
  		vfs_node_t *node2 = vfs_finddir(node, de->name);
  		if(node2 == NULL) goto cont;
  		puts(de->name);
cont:
  		free(de);
  		continue;
  	}
  } else puts(path_cwd);
}

int main(int argc, char **argv) {
  if(argc == 1) ls_primitive();
  else if(argc >= 2) {
    char cwd_orig[1024]; strcpy(cwd_orig, path_cwd);
    for(int32_t i = 1; i < argc; i++) {
      path_setcwd(argv[i]);
      if(argc > 2) printf("%s:\n", path_cwd);
      ls_primitive();
      if(argc > 2) putchar('\n');
      strcpy(path_cwd, cwd_orig);
    }
  }
  return 0;
}
