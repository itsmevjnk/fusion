/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <rtc.h>
#include <i386-pc/cmos.h>

uint8_t rtc_century_reg = 0; // for the code that deals with ACPI to fill in later

static uint8_t bcd2dec(uint8_t val) {
	return ((val & 0x0f) + ((val / 16) * 10));
}

rtc_t *rtc_gettime(rtc_t *dt) {
	rtc_t last_val; // used to try to get a consistent date-time information
	uint8_t century = 0, last_century = 0;
	do {
		last_val.day = dt->day; last_val.date = dt->date; last_val.month = dt->month; last_val.year = dt->year;
		last_val.hour = dt->hour; last_val.min = dt->min; last_val.sec = dt->sec;
		last_century = century;
		while(cmos_rtcupdate());
		dt->sec = cmos_read(0x00);
		dt->min = cmos_read(0x02);
		dt->hour = cmos_read(0x04);
		dt->day = cmos_read(0x06);
		dt->date = cmos_read(0x07);
		dt->month = cmos_read(0x08);
		dt->year = cmos_read(0x09);
		if(rtc_century_reg) century = cmos_read(rtc_century_reg);
	} while((last_val.day != dt->day) || (last_val.date != dt->date) || (last_val.month != dt->month) || (last_val.year != dt->year) ||
	        (last_val.hour != dt->hour) || (last_val.min != dt->min) || (last_val.sec != dt->sec) || (last_century != century));
	uint8_t regb = cmos_read(0x0b);
	if(!(regb & 0x04)) {
		dt->sec = bcd2dec(dt->sec);
		dt->min = bcd2dec(dt->min);
		dt->hour = bcd2dec(dt->hour);
		dt->date = bcd2dec(dt->date);
		dt->month = bcd2dec(dt->month);
		dt->year = bcd2dec(dt->year);
		if(rtc_century_reg) century = bcd2dec(century);
	}
	if(!(regb & 0x02) && (dt->hour & 0x80)) dt->hour = ((dt->hour & 0x7f) + 12) % 24;
	if(rtc_century_reg) dt->year += century * 100;
	else dt->year += 2000;
	if(!dt->day) dt->day = 1; // on some BIOSes (most notably VirtualBox) Sunday is returned as 0 instead of 1
	return dt;
}
