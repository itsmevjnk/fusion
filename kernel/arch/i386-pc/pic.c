/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <i386-pc/pic.h>
#include <i386-pc/idt.h>
#include <io.h>
#include <stdio.h>

uint8_t pic_offset0, pic_offset1; // interrupt offset
volatile uint64_t pic_spurious_7 = 0, pic_spurious_15 = 0; // spurious IRQ counter
void (*pic_irqhandlers[16])() = {NULL}; // array of pointers to IRQ handlers

void pic_eoi(uint8_t irq) {
	if(irq > 7) outb(0xA0, 0x20);
	outb(0x20, 0x20);
}

void pic_remap(uint8_t offset0, uint8_t offset1) {
	uint8_t mask0, mask1;
	mask0 = inb(0x21);
	mask1 = inb(0xA1);
	outb(0x20, 0x11); io_wait();
	outb(0xA0, 0x11); io_wait();
	outb(0x21, offset0); io_wait();
	outb(0xA1, offset1); io_wait();
	outb(0x21, 4); io_wait();
	outb(0xA1, 2); io_wait();
	outb(0x21, 1); io_wait();
	outb(0xA1, 1); io_wait();
	outb(0x21, mask0);
	outb(0xA1, mask1);
	pic_offset0 = offset0; pic_offset1 = offset1;
	asm("sti"); // finally enable interrupt since we've already covered all of the possible interrupt vectors by now
}

void pic_mask(uint8_t irq, uint8_t state) {
	uint16_t port;
	uint8_t val;
	if(irq < 8) port = 0x21;
	else {
		port = 0xA1;
		irq -= 8;
	}
	if(state) val = inb(port) | (1 << irq);
	else val = inb(port) & ~(1 << irq);
	outb(port, val);

}

void pic_mask_all(void) {
	outb(0x21, 0xFF);
	outb(0xA1, 0xFF);
}

void pic_unmask_all(void) {
	outb(0x21, 0);
	outb(0xA1, 0);
}

uint16_t pic_get_ocw3(uint8_t ocw3) {
	outb(0x20, ocw3);
	outb(0xA0, ocw3);
	return (inb(0xA0) << 8) | inb(0x20);
}

uint16_t pic_get_irr(void) {
	return pic_get_ocw3(0x0A);
}

uint16_t pic_get_isr(void) {
	return pic_get_ocw3(0x0B);
}

static void pic_handler_stub(uint8_t irq) {
	// printf("IRQ %u fired\n", irq);
	uint16_t isr = pic_get_isr();
	/* check if it's a spurious IRQ */
	if((irq == 7) && !(isr & (1 << 7))) {
		pic_spurious_7++;
		return;
	}
	if((irq == 15) && !(isr & (1 << 15))) {
		pic_spurious_15++;
		pic_eoi(0); // just some random master PIC IRQ line here
		return;
	}
	/* call the corresponding IRQ handler */
	if(pic_irqhandlers[irq] == NULL) return;
	// pic_eoi(irq); // never forget to send an EOI!
	(*pic_irqhandlers[irq])();
}

void pic_handler_master(uint8_t vector, idt_regs_t *regs) {
	(void) regs;
	pic_handler_stub(vector - pic_offset0);
}

void pic_handler_slave(uint8_t vector, idt_regs_t *regs) {
	(void) regs;
	pic_handler_stub(vector - pic_offset1 + 8);
}

void pic_setup_handler(void) {
	for(uint8_t i = 0; i <= 7; i++) idt_add_handler(pic_offset0 + i, pic_handler_master);
	for(uint8_t i = 0; i <= 7; i++) idt_add_handler(pic_offset1 + i, pic_handler_slave);
}
