/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <textmode.h>
#include <i386-pc/textmode.h>
#include <io.h>
#include <font.h>
#include <psf.h>
#include <stdlib.h>

uint8_t text_attribs = 0x07;
uint32_t text_bg = rgb(0, 0, 0), text_fg = rgb(255, 255, 255), text_x = 0, text_y = 0, text_xoff = 0, text_yoff = 0;
fb_device_t *text_fb = NULL;
static uint8_t *text_buf = (uint8_t*) 0xB8000;
/* adapted from https://en.wikipedia.org/wiki/Color_Graphics_Adapter */
static uint32_t colors[16] = {0x000000, 0x0000AA, 0x00AA00, 0x00AAAA, 0xAA0000, 0xAA00AA, 0xAA5500, 0xAAAAAA,
							  0x555555, 0x5555FF, 0x55FF55, 0x55FFFF, 0xFF5555, 0xFF55FF, 0xFFFF55, 0xFFFFFF};

#define FONT_WIDTH							((psf_glyphs == NULL) ? 8 : psf_width)
#define FONT_HEIGHT							((psf_glyphs == NULL) ? 8 : psf_height)



static void text_update(void) {
	if(text_fb == NULL) {
		uint16_t pos = text_y * 80 + text_x;
		outb(0x3d4, 0x0f);
		outb(0x3d5, (uint8_t) (pos & 0xff));
		outb(0x3d4, 0x0e);
		outb(0x3d5, (uint8_t) ((pos >> 8) & 0xff));
	}
}

void text_writec(uint32_t c) {
	if(text_fb == NULL) {
		text_buf[(text_y * 80 + text_x) * 2] = c;
		text_buf[(text_y * 80 + text_x) * 2 + 1] = text_attribs;
		text_update();
	} else if(psf_glyphs == NULL) {
		for(uint8_t y = 0; y < 8; y++) {
			uint8_t t = font_8x8[c * 8 + y];
			for(uint8_t x = 0; x < 8; x++) {
				if(t & 1) fb_putpixel(text_fb, text_x * 8 + x, text_y * 8 + y, text_fg);
				else fb_putpixel(text_fb, text_x * 8 + x, text_y * 8 + y, text_bg);
				t >>= 1;
			}
		}
	} else psf_writec(c, text_x * FONT_WIDTH, text_y * FONT_HEIGHT, text_bg, text_fg);
}

uint32_t text_get_xres(void) {
	if(text_fb != NULL) return text_fb->width / FONT_WIDTH;
	else return 80;
}

uint32_t text_get_yres(void) {
	if(text_fb != NULL) return text_fb->height / FONT_HEIGHT;
	return 25;
}
void text_clrline(void) {
	if(text_fb == NULL) {
		for(uint8_t x = 0; x < 80; x++) {
			text_buf[(text_y * 80 + x) * 2] = 0;
			text_buf[(text_y * 80 + x) * 2 + 1] = 0x07;
		}
		text_update();
	} else {
		fb_fillrect(text_fb, 0, text_y * FONT_HEIGHT, text_fb->width - 1, text_y * FONT_HEIGHT + FONT_HEIGHT - 1, rgb(0, 0, 0));
	}
}

void text_clear(void) {
	if(text_fb == NULL) {
		for(uint8_t y = 0; y < 25; y++) {
			for(uint8_t x = 0; x < 80; x++) {
				text_buf[(y * 80 + x) * 2] = 0;
				text_buf[(y * 80 + x) * 2 + 1] = 0x07;
			}
		}
		// text_update();
	} else {
		fb_fill(text_fb, rgb(0, 0, 0));
	}
	text_x = 0; text_y = 0;
	text_update();
}

void text_scrollup(uint32_t lines, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1) {
	if(lines == 0 || lines >= text_get_yres()) text_clear();
	else if(text_fb == NULL) {
		for(uint8_t i = 0; i < lines; i++) {
			/* copy each line to the previous line */
			for(uint8_t y = y0 + 1; y <= y1; y++) {
				for(uint8_t x = x0; x <= x1; x++) {
					text_buf[((y - 1) * 80 + x) * 2] = text_buf[(y * 80 + x) * 2];
					text_buf[((y - 1) * 80 + x) * 2 + 1] = text_buf[(y * 80 + x) * 2 + 1];
				}
			}
			/* wipe out the last line */
			for(uint8_t x = x0; x <= x1; x++) {
				text_buf[(y1 * 80 + x) * 2] = 0;
				text_buf[(y1 * 80 + x) * 2 + 1] = 0x07;
			}
		}
	} else if(x0 == 0 && x1 == text_get_xres() - 1) fb_scrollup(text_fb, lines * FONT_HEIGHT, y0 * FONT_HEIGHT, (y1 + 1) * FONT_HEIGHT - 1);
	else {
		for(uint32_t n = 0; n < lines; n++) {
			for(uint32_t y = (y0 + 1) * FONT_HEIGHT; y < (y1 + 1) * FONT_HEIGHT; y++) {
				for(uint32_t x = x0 * FONT_WIDTH; x < (x1 + 1) * FONT_WIDTH; x++) fb_putpixel(text_fb, x, y - FONT_HEIGHT, fb_getpixel(text_fb, x, y));
			}
			for(uint32_t y = y1 * FONT_HEIGHT; y < (y1 + 1) * FONT_HEIGHT; y++) {
				for(uint32_t x = x0 * FONT_WIDTH; x < (x1 + 1) * FONT_WIDTH; x++) fb_putpixel(text_fb, x, y, rgb(0, 0, 0));
			}
		}
	}
}

void text_scrolldown(uint32_t lines, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1) {
	if(lines == 0 || lines >= text_get_yres()) text_clear();
	else if(text_fb == NULL) {
		for(uint8_t i = 0; i < lines; i++) {
			/* copy each line to the next line */
			for(uint8_t y = y1 - 1; y >= y0; y--) {
				for(uint8_t x = x0; x <= x1; x++) {
					text_buf[((y + 1) * 80 + x) * 2] = text_buf[(y * 80 + x) * 2];
					text_buf[((y + 1) * 80 + x) * 2 + 1] = text_buf[(y * 80 + x) * 2 + 1];
				}
			}
			/* wipe out the first line */
			for(uint8_t x = x0; x <= x1; x++) {
				text_buf[(y0 * 80 + x) * 2] = 0;
				text_buf[(y0 * 80 + x) * 2 + 1] = 0x07;
			}
		}
	} else {
		for(uint32_t n = 0; n < lines; n++) {
			for(int y = (y1 - 1) * FONT_HEIGHT; y >= (int) (y0 * FONT_HEIGHT); y--) {
				for(uint32_t x = x0 * FONT_WIDTH; x < (x1 + 1) * FONT_WIDTH; x++) fb_putpixel(text_fb, x, y, fb_getpixel(text_fb, x, y + FONT_HEIGHT));
			}
			for(uint32_t y = y0 * FONT_HEIGHT; y < ((y0 + 1) * FONT_HEIGHT); y++) {
				for(uint32_t x = x0 * FONT_WIDTH; x < (x1 + 1) * FONT_WIDTH; x++) fb_putpixel(text_fb, x, y, rgb(0, 0, 0));
			}
		}
	}
}

void text_putc_common(uint32_t c) {
	if(c == '\n' || c == '\r') {
		text_x = 0;
		text_y++;
		if(text_y > text_get_yres() - 1) {
			text_y = text_get_yres() - 1;
			text_scrollup(1, 0, 0, text_get_xres() - 1, text_get_yres() - 1);
		}
	} else if(c == '\b') {
		if(text_x) text_x--;
		else {
			text_x = text_get_xres() - 1;
			if(text_y) text_y--;
			else text_scrolldown(1, 0, 0, text_get_xres() - 1, text_get_yres() - 1);
		}
	} else if(c == '\t') {
		text_x += 4 - (text_x % 4);
		if(text_x > text_get_xres() - 1) {
			text_x = 0;
			text_y++;
			if(text_y > text_get_yres() - 1) {
				text_y = text_get_yres() - 1;
				text_scrollup(1, 0, 0, text_get_xres() - 1, text_get_yres() - 1);
			}
		}
	} else {
		text_writec(c);
		text_x++;
		if(text_x > text_get_xres() - 1) {
			text_x = 0;
			text_y++;
			if(text_y > text_get_yres() - 1) {
				text_y = text_get_yres() - 1;
				text_scrollup(1, 0, 0, text_get_xres() - 1, text_get_yres() - 1);
			}
		}
	}
}

void text_putc(uint32_t c) {
	text_putc_common(c);
	text_update();
}

void text_puts(const char *s) {
	for(uint32_t i = 0; s[i] != 0; i++) text_putc_common((uint8_t) s[i]);
	text_update();
}

void text_setbg(uint8_t bg) {
	if(text_fb == NULL) {
		text_attribs = (text_attribs & 0x0F) | ((bg & 0x0F) << 4);
		/* make sure blinking is disabled */
		inb(0x3DA);
		uint8_t t = inb(0x3C0);
		outb(0x3C0, 0x10);
		outb(0x3C0, (inb(0x3C1) & ~(1 << 3)));
		outb(0x3C0, t);
	} else text_bg = colors[bg];
}

void text_setfg(uint8_t fg) {
	if(text_fb == NULL) {
		text_attribs = (text_attribs & 0xF0) | (fg & 0x0F);
	} else text_fg = colors[fg];
}

uint8_t text_getbg(void) {
	if(text_fb == NULL) return ((text_attribs >> 4) & 0x0F);
	else {
		for(uint8_t i = 0; i < 16; i++) {
			if(colors[i] == text_bg) return colors[i];
		}
		return 0;
	}
}

uint8_t text_getfg(void) {
	if(text_fb == NULL) return (text_attribs & 0x0F);
	else {
		for(uint8_t i = 0; i < 16; i++) {
			if(colors[i] == text_fg) return colors[i];
		}
		return 0;
	}
}

void text_cursor_on(uint8_t begin, uint8_t end) {
	if(text_fb != NULL) return;
	outb(0x3d4, 0x0a);
	outb(0x3d5, (inb(0x3d5) & 0xc0) | begin);
	outb(0x3d4, 0x0b);
	outb(0x3d5, (inb(0x3d5) & 0xe0) | end);
}

void text_cursor_off(void) {
	if(text_fb != NULL) return;
	outb(0x3d4, 0x0a);
	outb(0x3d5, 0x20);
}

void text_setxy(uint32_t x, uint32_t y) {
	text_x = x; text_y = y;
	if(text_fb == NULL) text_update();
}

void text_getxy(uint32_t *x, uint32_t *y) {
	*x = text_x; *y = text_y;
}

/* text data backup/restore */
uint32_t text_data_size(void) {
	return sizeof(text_data_t);
}

void text_data_save(void *dst) {
	text_data_t *dat = (text_data_t*) dst;
	dat->x = text_x; dat->y = text_y; dat->attr = text_attribs; dat->bg = text_bg; dat->fg = text_fg;
	dat->dev = text_fb;
}

void text_data_load(void *src) {
	text_data_t *dat = (text_data_t*) src;
	text_x = dat->x; text_y = dat->y; text_attribs = dat->attr; text_bg = dat->bg; text_fg = dat->fg;
	text_fb = dat->dev;
}
