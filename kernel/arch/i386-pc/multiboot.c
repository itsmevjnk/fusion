/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <i386-pc/multiboot.h>
#include <placement.h>
#include <assert.h>
#include <string.h>

uintptr_t mb_start, mb_end;
mb_info_t *mb_info = NULL;

extern uint32_t kernel_end;

void mb_init(void) {
	assert(mb_info != NULL);
	mb_start = (uintptr_t) mb_info + sizeof(mb_info_t);
	if(mb_info->flags & (1 << 3)) {
		/* mods_count and mods_addr */
		if(mb_info->mods_count && mb_info->mods_addr != NULL) {
			mb_module_t *module = mb_info->mods_addr;
			for(uint32_t i = 0; i < mb_info->mods_count; i++) {
				if((uintptr_t) module + sizeof(mb_module_t) > mb_start) mb_start = (uintptr_t) module + sizeof(mb_module_t);
				if(module->mod_end > mb_start) mb_start = module->mod_end;
				if(module->string != NULL && (uintptr_t) module->string + strlen((const char*) module->string) > mb_start) mb_start = (uintptr_t) module->string + strlen((const char*) module->string);
				module = (mb_module_t*) ((uintptr_t) module + sizeof(mb_module_t));
			}
		}
	}
	if(mb_info->flags & (1 << 6)) {
		/* mmap_length and mmap_addr */
		if(mb_info->mmap_length && mb_info->mmap_addr != NULL) {
			if((uintptr_t) mb_info->mmap_addr + mb_info->mmap_length > mb_start) mb_start = (uintptr_t) mb_info->mmap_addr + mb_info->mmap_length;
		}
	}
	/* NOTE: Anything other than the modules and memory map are not needed as of now and so we can discard them for simplicity, TODO: add them in */
	placement_addr = (mb_start > (uintptr_t) &kernel_end) ? mb_start : (uintptr_t) &kernel_end;
	/* here goes the relocation */
	uint32_t new_addr = pmalloc(sizeof(mb_info_t), 0);
	memcpy((void*) new_addr, mb_info, sizeof(mb_info_t));
	mb_info = (mb_info_t*) new_addr;
	if(mb_info->flags & (1 << 3)) {
		/* mods_count and mods_addr */
		if(mb_info->mods_count && mb_info->mods_addr != NULL) {
			new_addr = pmalloc(mb_info->mods_count * sizeof(mb_module_t), 0);
			memcpy((void*) new_addr, mb_info->mods_addr, mb_info->mods_count * sizeof(mb_module_t));
			mb_info->mods_addr = (mb_module_t*) new_addr;
			mb_module_t *module = mb_info->mods_addr;
			for(uint32_t i = 0; i < mb_info->mods_count; i++) {
				uintptr_t size = module->mod_end - module->mod_start;
				new_addr = pmalloc(size, 0);
				memcpy((void*) new_addr, (void*) module->mod_start, size);
				module->mod_start = new_addr;
				module->mod_end = new_addr + size;
				if(module->string != NULL) {
					new_addr = pmalloc(strlen((const char*) module->string), 0);
					memcpy((void*) new_addr, module->string, strlen((const char*) module->string));
					module->string = (uint8_t*) new_addr;
				}
				module = (mb_module_t*) ((uintptr_t) module + sizeof(mb_module_t));
			}
		}
	}
	if(mb_info->flags & (1 << 6)) {
		/* mmap_length and mmap_addr */
		if(mb_info->mmap_length && mb_info->mmap_addr != NULL) {
			new_addr = pmalloc(mb_info->mmap_length, 0);
			memcpy((void*) new_addr, mb_info->mmap_addr, mb_info->mmap_length);
			mb_info->mmap_addr = (mb_mmap_t*) new_addr;
		}
	}
	mb_end = placement_addr;
}
