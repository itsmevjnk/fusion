; Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).

; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

global paging_copy_frame
paging_copy_frame:
push ebx
pushf
cli
mov ebx, [esp + 12]
mov ecx, [esp + 16]
mov edx, cr0
and edx, 0x7fffffff
mov cr0, edx
mov edx, 1024
.loop:
mov eax, [ebx]
mov [ecx], eax
add ebx, 4
add ecx, 4
dec edx
jnz .loop
mov edx, cr0
or edx, 0x80000000
mov cr0, edx
popf
pop ebx
ret
