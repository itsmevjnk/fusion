/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <power.h>
#include <lai/helpers/pm.h>
#include <i386-pc/ps2.h>
#include <i386-pc/idt.h>

void power_shutdown(void) {
	lai_enter_sleep(5);
	asm("hlt");
}

void power_reboot(void) {
	ps2_reboot();
	idt_reboot(); // last ditch effort, if all fails
	asm("hlt");
}
