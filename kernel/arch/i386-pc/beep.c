/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
#include <beep.h>
#include <io.h>

void beep(uint16_t freq) {
	if(!freq) {
		// make the speaker shut up
		uint8_t tmp = inb(0x61) & 0xFC;
		outb(0x61, tmp);
	} else {
		uint32_t div = 1193180 / freq;
		outb(0x43, 0xB6);
		outb(0x42, (uint8_t) div);
		outb(0x42, (uint8_t) (div >> 8));
		uint8_t tmp = inb(0x61) | 3;
		outb(0x61, tmp);
	}
}