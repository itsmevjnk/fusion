/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdint.h>
#include <pfa.h>
#include <string.h>
#include <stdlib.h>
#include <placement.h>

typedef struct {
	uint8_t present : 1;
	uint8_t rw : 1;
	uint8_t user : 1;
	uint8_t wthru : 1;
	uint8_t cache_disable : 1;
	uint8_t accessed : 1;
	uint8_t zero : 1;
	uint8_t page_size : 1;
	uint8_t ignored : 1;
	uint8_t available : 3;
	uint32_t phys_pt : 20;
} __attribute__ ((packed)) paging_pde_t;

typedef struct {
	uint8_t present : 1;
	uint8_t rw : 1;
	uint8_t user : 1;
	uint8_t wthru : 1;
	uint8_t cache_disable : 1;
	uint8_t accessed : 1;
	uint8_t dirty : 1;
	uint8_t page_size : 1;
	uint8_t global : 1;
	uint8_t available : 3;
	uint32_t phys_addr : 20;
} __attribute__ ((packed)) paging_pte_t;

typedef struct {
	paging_pte_t entries[1024];
} __attribute__ ((packed)) paging_pt_t;

typedef struct {
	paging_pt_t *tables[1024];
	paging_pde_t entries[1024];
	uint32_t phys_pd;
} __attribute__ ((packed)) paging_pd_t;

paging_pd_t *kernel_dir = NULL;
paging_pd_t *current_dir = NULL;

extern uint32_t kernel_start;
extern uint32_t text_start;
extern uint32_t text_end;
extern uint32_t rodata_start;
extern uint32_t rodata_end;
extern uint32_t data_start;
extern uint32_t data_end;
extern uint32_t bss_start;
extern uint32_t bss_end;
extern uint32_t kernel_end;

uint8_t paging_readable(uintptr_t vaddr) {
	uint32_t pd_idx = vaddr >> 22, pt_idx = (vaddr >> 12) & 0x3FF;
	if(!current_dir->entries[pd_idx].present) return 0;
	paging_pte_t *pt = (paging_pte_t*) (current_dir->tables[pd_idx]);
	return pt[pt_idx].present;
}

uint8_t paging_writable(uintptr_t vaddr) {
	uint32_t pd_idx = vaddr >> 22, pt_idx = (vaddr >> 12) & 0x3FF;
	if(!current_dir->entries[pd_idx].present || !current_dir->entries[pd_idx].rw) return 0;
	paging_pte_t *pt = (paging_pte_t*) (current_dir->tables[pd_idx]);
	return pt[pt_idx].rw;
}

uintptr_t paging_getpaddr(uintptr_t vaddr) {
	uint32_t pd_idx = vaddr >> 22, pt_idx = (vaddr >> 12) & 0x3FF, offset = vaddr & 0xFFF;
	if(!current_dir->entries[pd_idx].present) return 0;
	paging_pte_t *pt = (paging_pte_t*) (current_dir->tables[pd_idx]);
	if(!pt[pt_idx].present) return 0;
	return (pt[pt_idx].phys_addr << 12) + offset;
}

static inline void invlpg(uint32_t m) {
	asm volatile("invlpg (%0)" : : "b"(m) : "memory");
}

void paging_map(uintptr_t paddr, uintptr_t vaddr, uintptr_t size, uint8_t is_user, uint8_t is_writable) {
	uintptr_t paddr_a = paddr, vaddr_a = vaddr, poff = 0, voff = 0;
	if(paddr & 0xFFF) {
		paddr_a &= ~0xFFF;
		poff = paddr - paddr_a;
	}
	if(vaddr & 0xFFF) {
		vaddr_a &= ~0xFFF;
		voff = vaddr - vaddr_a;
	}
	paddr = paddr_a; vaddr = vaddr_a;
	size += (poff >= voff) ? poff : voff;
	for(uintptr_t i = 0; i < size; i += 0x1000) {
		uint32_t pd_idx = vaddr >> 22, pt_idx = (vaddr >> 12) & 0x3FF;
		pfa_setframe(paddr >> 12);
		if(!current_dir->entries[pd_idx].present) {
			uintptr_t phys;
			paging_pt_t *table;
			if(heap_info == NULL) {
				table = (paging_pt_t*) pmalloc(sizeof(paging_pt_t), 1);
				phys = (uintptr_t) table;
			} else table = (paging_pt_t*) malloc_ext(sizeof(paging_pt_t), 4096, &phys);
			memset(table, 0, sizeof(paging_pt_t));
			current_dir->entries[pd_idx].present = 1;
			current_dir->entries[pd_idx].rw = is_writable;
			current_dir->entries[pd_idx].user = is_user;
			current_dir->entries[pd_idx].phys_pt = phys >> 12;
			current_dir->tables[pd_idx] = table;
		}
		paging_pte_t *pt = (paging_pte_t*) (current_dir->tables[pd_idx]);
		pt[pt_idx].present = 1;
		pt[pt_idx].rw = is_writable;
		pt[pt_idx].user = is_user;
		pt[pt_idx].phys_addr = paddr >> 12;
		invlpg(current_dir->entries[pd_idx].phys_pt << 12);
		paddr += 0x1000; vaddr += 0x1000;
	}
}

void paging_unmap(uintptr_t vaddr, uintptr_t size) {
	uintptr_t vaddr_a = vaddr;
	if(vaddr & 0xFFF) {
		vaddr_a &= ~0xFFF;
		size += vaddr - vaddr_a;
	}
	vaddr = vaddr_a;
	for(uintptr_t i = 0; i < size; i += 0x1000) {
		uint32_t pd_idx = vaddr >> 22, pt_idx = (vaddr >> 12) & 0x3FF;
		pfa_clrframe(paging_getpaddr(vaddr >> 12));
		if(current_dir->entries[pd_idx].present) {
			paging_pte_t *pt = (paging_pte_t*) (current_dir->tables[pd_idx]);
			pt[pt_idx].present = 0;
			invlpg(current_dir->entries[pd_idx].phys_pt << 12);
		}
		vaddr += 0x1000;
	}
}

void paging_enable(void) {
	uint32_t cr0;
	asm volatile("mov %%cr0, %0" : "=r"(cr0));
	cr0 |= (1 << 31);
	asm volatile("mov %0, %%cr0" : : "r"(cr0));
}

void paging_disable(void) {
	uint32_t cr0;
	asm volatile("mov %%cr0, %0" : "=r"(cr0));
	cr0 &= ~(1 << 31);
	asm volatile("mov %0, %%cr0" : : "r"(cr0));
}

void paging_switch(uintptr_t id) {
	paging_pd_t *pd = (paging_pd_t*) id;
	current_dir = pd;
	asm volatile("mov %0, %%cr3" : : "r"(pd->phys_pd));
}

uintptr_t paging_id2pd(uintptr_t id) {
	paging_pd_t *pd = (paging_pd_t*) id;
	return pd->phys_pd;
}

static void paging_alloc_frame(paging_pte_t *page, uint8_t is_user, uint8_t is_writable) {
	if(page->phys_addr) return;
	uint32_t frame = pfa_freeframe();
	if(frame == 0xFFFFFFFF) return;
	pfa_setframe(frame);
	page->present = 1;
	page->rw = is_writable;
	page->user = is_user;
	page->phys_addr = frame;
}

extern void paging_copy_frame(uint32_t src, uint32_t dest);

static paging_pt_t *paging_clone_tab(paging_pt_t *src, uint32_t *phys) {
	paging_pt_t *table = (paging_pt_t*) malloc_ext(sizeof(paging_pt_t), 4096, phys);
	memset(table, 0, sizeof(paging_pt_t));
	for(uint16_t i = 0; i < 1024; i++) {
		if(!src->entries[i].phys_addr) continue;
		paging_alloc_frame(&table->entries[i], 1, 0);
		table->entries[i].present = src->entries[i].present;
		table->entries[i].rw = src->entries[i].rw;
		table->entries[i].user = src->entries[i].user;
		table->entries[i].accessed = src->entries[i].accessed;
		table->entries[i].dirty = src->entries[i].dirty;
		paging_copy_frame(src->entries[i].phys_addr * 0x1000, table->entries[i].phys_addr * 0x1000);
	}
	return table;
}

uintptr_t paging_clone(uintptr_t src) {
	paging_pd_t *src_pd = (paging_pd_t*) src;
	uintptr_t phys;
	paging_pd_t *tgt_pd = (paging_pd_t*) malloc_ext(sizeof(paging_pd_t), 4096, &phys);
	memset(tgt_pd, 0, sizeof(paging_pd_t));
	tgt_pd->phys_pd = phys + (uintptr_t) tgt_pd->entries - (uintptr_t) tgt_pd;
	for(uint16_t i = 0; i < 1024; i++) {
		if(!src_pd->tables[i]) continue;
		if(kernel_dir->tables[i] == src_pd->tables[i]) {
			tgt_pd->tables[i] = src_pd->tables[i];
			tgt_pd->entries[i] = src_pd->entries[i];
		} else {
			tgt_pd->tables[i] = paging_clone_tab(src_pd->tables[i], &phys);
			tgt_pd->entries[i].phys_pt = phys;
			tgt_pd->entries[i].present = 1;
			tgt_pd->entries[i].rw = 1;
			tgt_pd->entries[i].user = 1;
		}
	}
	return (uintptr_t) tgt_pd;
}

void paging_init(void) {
	kernel_dir = (paging_pd_t*) pmalloc(sizeof(paging_pd_t), 1);
	memset(kernel_dir, 0, sizeof(paging_pd_t));
	kernel_dir->phys_pd = (uint32_t) &kernel_dir->entries;
	current_dir = kernel_dir;
	paging_map(0, 0, (uintptr_t) &kernel_start, 0, 0); // map the first 1M as sacred land
	paging_map((uintptr_t) &text_start, (uintptr_t) &text_start, (uintptr_t) &text_end - (uintptr_t) &text_start, 0, 0); // more sacred land
	paging_map((uintptr_t) &rodata_start, (uintptr_t) &rodata_start, (uintptr_t) &rodata_end - (uintptr_t) &rodata_start, 0, 0); // more of those
	paging_map((uintptr_t) &data_start, (uintptr_t) &data_start, (uintptr_t) &data_end - (uintptr_t) &data_start, 0, 1); // .data has to be writable
	paging_map((uintptr_t) &bss_start, (uintptr_t) &bss_start, (uintptr_t) &bss_end - (uintptr_t) &bss_start, 0, 1); // same thing with .bss
	paging_map((uintptr_t) &kernel_end, (uintptr_t) &kernel_end, placement_addr - (uintptr_t) &kernel_end + 0x8000, 0, 1); // for the stuff allocated using pmalloc()
	paging_switch((uintptr_t) kernel_dir);
	paging_enable();
}

uintptr_t paging_find_free(uintptr_t size) {
	for(uint64_t i = 0x100000; i <= (uint64_t) ((uint64_t) 0x100000000 - size); i += 0x1000) {
		uintptr_t i32 = (uintptr_t) i;
		if(!paging_readable(i32)) {
			uint8_t t = 0;
			for(uintptr_t j = 0; j < size; j += 0x1000) {
				if(paging_readable(i32 + j)) {
					t = 1;
					break;
				}
			}
			if(!t) {
				// printf("found free slot at 0x%x\n", i32);
				return i32;
			}
		}
	}
	return 0;
}
