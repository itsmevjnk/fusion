; Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).

; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.

; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.

%define MAGIC 0x1BADB002
%define FLAGS ((1 << 0) | (1 << 1))
%define CHECKSUM -(MAGIC + FLAGS)

section .multiboot
align 4
dd MAGIC ; magic number
dd FLAGS ; flags
dd CHECKSUM ; checksum

section .bss
align 16
stack_bottom:
resb 16384
stack_top:

; actual code
section .text
global _start
extern _init
extern kainit
extern kmain
extern mb_info ; declared in arch/i386-pc/multiboot.c
extern initrd_addr ; declared in kernel/i386-pc.c
extern initrd_size ; same as above
extern exit
_start:
mov [mb_info], ebx
mov esp, stack_top
call _init ; call global constructor to let it initialize stuff for us
call kainit ; call architecture-specific initialization code
push dword [initrd_size]
push dword [initrd_addr]
call kmain ; call kernel main code
jmp exit
