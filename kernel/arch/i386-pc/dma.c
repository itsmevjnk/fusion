/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
#include <i386-pc/dma.h>
#include <io.h>

void dma_set_addr(uint8_t channel, uint16_t addr) {
	if(channel >= 8) return;
	uint8_t port = 0;
	switch(channel) {
		case 0: port = DMA0_REG_ADDR_0; break;
		case 1: port = DMA0_REG_ADDR_1; break;
		case 2: port = DMA0_REG_ADDR_2; break;
		case 3: port = DMA0_REG_ADDR_3; break;
		case 4: port = DMA1_REG_ADDR_4; break;
		case 5: port = DMA1_REG_ADDR_5; break;
		case 6: port = DMA1_REG_ADDR_6; break;
		case 7: port = DMA1_REG_ADDR_7; break;
	}
	outb(port, (uint8_t) (addr & 0xFF));
	outb(port, (uint8_t) (addr >> 8));
}

void dma_set_cnt(uint8_t channel, uint16_t cnt) {
	if(channel >= 8) return;
	uint8_t port = 0;
	switch(channel) {
		case 0: port = DMA0_REG_CNT_0; break;
		case 1: port = DMA0_REG_CNT_1; break;
		case 2: port = DMA0_REG_CNT_2; break;
		case 3: port = DMA0_REG_CNT_3; break;
		case 4: port = DMA1_REG_CNT_4; break;
		case 5: port = DMA1_REG_CNT_5; break;
		case 6: port = DMA1_REG_CNT_6; break;
		case 7: port = DMA1_REG_CNT_7; break;
	}
	outb(port, (uint8_t) (cnt & 0xFF));
	outb(port, (uint8_t) (cnt >> 8));
}

void dma_set_page(uint8_t channel, uint8_t page) {
	if(channel >= 8) return;
	uint8_t port = 0;
	switch(channel) {
		case 1: port = DMA_REG_PAGE_1; break;
		case 2: port = DMA_REG_PAGE_2; break;
		case 3: port = DMA_REG_PAGE_3; break;
		case 5: port = DMA_REG_PAGE_5; break;
		case 6: port = DMA_REG_PAGE_6; break;
		case 7: port = DMA_REG_PAGE_7; break;
		default: return;
	}
	outb(port, page);
}

void dma_mask(uint8_t channel) {
	if(channel >= 8) return;
	outb((channel < 4) ? DMA0_REG_CHMASK : DMA1_REG_CHMASK, (1 << (channel & 0x03)));
}

void dma_unmask(uint8_t channel) {
	if(channel >= 8) return;
	outb((channel < 4) ? DMA0_REG_CHMASK : DMA1_REG_CHMASK, (channel & 0x03));
}

void dma_set_mode(uint8_t channel, uint8_t mode) {
	if(channel >= 8) return;
	dma_mask(channel);
	outb(((channel < 4) ? DMA0_REG_MODE : DMA1_REG_MODE), (channel & 0x03) | mode);
	dma_unmask(channel);
}

void dma_reset_ff(uint8_t dma) {
	if(dma >= 2) return;
	outb((dma) ? DMA1_REG_CLRFF : DMA0_REG_CLRFF, 0xFF);
}

void dma_reset(uint8_t dma) {
	if(dma >= 2) return;
	outb((dma) ? DMA1_REG_TEMP : DMA0_REG_TEMP, 0xFF);
}

void dma_unmask_all(uint8_t dma) {
	if(dma >= 2) return;
	outb((dma) ? DMA1_REG_CLRMASK : DMA0_REG_CLRMASK, 0xFF);
}