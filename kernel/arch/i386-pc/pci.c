/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <i386-pc/pci.h>
#include <i386-pc/int32.h>
#include <io.h>

uint8_t pci_csam = 0; // configuration space access mechanism (0 = no PCI)

int pci_init(void) {
	/* TODO: check if there's PCI */
	pci_csam = 1;
	return 0;
}

uint8_t pci_config_read8(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset) {
	if(pci_csam != 1) return 0;
	outl(0xCF8, (bus << 16) | (slot << 11) | (func << 8) | (offset & 0xFC) | (1 << 31));
	return inb(0xCFC + (offset & 3));
}

uint16_t pci_config_read16(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset) {
	if(pci_csam != 1) return 0;
	outl(0xCF8, (bus << 16) | (slot << 11) | (func << 8) | (offset & 0xFC) | (1 << 31));
	return inw(0xCFC + (offset & 2));
}

uint32_t pci_config_read32(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset) {
	if(pci_csam != 1) return 0;
	outl(0xCF8, (bus << 16) | (slot << 11) | (func << 8) | (offset & 0xFC) | (1 << 31));
	return inl(0xCFC);
}

void pci_config_write8(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset, uint8_t val) {
	if(pci_csam != 1) return;
	outl(0xCF8, (bus << 16) | (slot << 11) | (func << 8) | (offset & 0xFC) | (1 << 31));
	outb(0xCFC + (offset & 3), val);
}

void pci_config_write16(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset, uint16_t val) {
	if(pci_csam != 1) return;
	outl(0xCF8, (bus << 16) | (slot << 11) | (func << 8) | (offset & 0xFC) | (1 << 31));
	outw(0xCFC + (offset & 2), val);
}

void pci_config_write32(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset, uint32_t val) {
	if(pci_csam != 1) return;
	outl(0xCF8, (bus << 16) | (slot << 11) | (func << 8) | (offset & 0xFC) | (1 << 31));
	outl(0xCFC, val);
}
