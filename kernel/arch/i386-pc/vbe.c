/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <i386-pc/vbe.h>
#include <i386-pc/int32.h>
#include <paging.h>
#include <string.h>
#include <stdlib.h>
#include <textmode.h>
#include <stdio.h>
#include <keyboard.h>
#include <timer.h>
#include <task.h>

typedef struct {
	uint8_t signature[4]; // must be "VESA"
	uint16_t version;
	uint32_t oem; // segment:offset pointer to OEM name
	uint32_t capabilities;
	uint32_t modes; // segment:offset pointer to list of supported video modes
	uint16_t mem; // memory size in 64K blocks
	uint16_t oem_sw_rev;
	uint32_t oem_vendor; // segment:offset pointer to card vendor string
	uint32_t oem_prod_name; // segment:offset pointer to model name
	uint32_t oem_prod_rev; // segment:offset pointer to model revision
	uint8_t reserved[222];
	uint8_t oem_data[256];
} __attribute__ ((packed)) vbe_ctrlinfo_t;

typedef struct {
	uint16_t attribs;
	uint8_t win_a, win_b;
	uint16_t granularity;
	uint16_t win_size;
	uint16_t seg_a, seg_b;
	uint32_t win_func_ptr;
	uint16_t pitch;

	uint16_t width, height;
	uint8_t w_char, y_char, planes, bpp, banks;
	uint8_t mem_model, bank_size, img_pages;
	uint8_t reserved;

	uint8_t r_mask, r_pos;
	uint8_t g_mask, g_pos;
	uint8_t b_mask, b_pos;
	uint8_t res_mask, res_pos;
	uint8_t direct_color_attribs;

	uint32_t physbase;
	uint32_t offscreenmem_off;
	uint32_t offscreenmem_size;
	uint8_t reserved2[206];
} __attribute__ ((packed)) vbe_modeinfo_t;

#define SEGOFF_PTR(seg, off)			((seg << 4) | off)
#define VBE_PTR(arr)					SEGOFF_PTR(((arr & 0xFFFF0000) >> 16), (arr & 0xFFFF))

uint32_t vbe_width, vbe_height, vbe_depth, vbe_pitch, vbe_bufsize;
uint8_t *vbe_buffer = NULL;

vbe_ctrlinfo_t vbe_ctrlinfo;
vbe_modeinfo_t vbe_modeinfo;

fb_device_t vbe_device;

static uint16_t vbe_find_mode(uint32_t width, uint32_t height, uint32_t depth) {
	uint16_t *modes = (uint16_t*) VBE_PTR(vbe_ctrlinfo.modes);
	for(uint32_t i = 0; modes[i] != 0xFFFF; i++) {
		vbe_modeinfo_t *mode = (vbe_modeinfo_t*) 0x3000;
		int32_regs_t regs;
		regs.ax = 0x4F01;
		regs.cx = modes[i];
		regs.es = 0x0000;
		regs.di = 0x3000;
		int32(0x10, &regs);
		if(mode->width == width && mode->height == height && mode->bpp == depth) return modes[i];
	}
	return 0; // cannot find video mode
}

int vbe_init(uint32_t width, uint32_t height, uint32_t depth) {
	int32_regs_t regs;
	regs.ax = 0x4F00;
	regs.es = 0x0000;
	regs.di = 0x2000;
	int32(0x10, &regs);
	// if((regs.ax & 0xFF) != 0x4F) return -1; // VBE not supported
	memcpy((void*) &vbe_ctrlinfo, (void*) 0x2000, sizeof(vbe_ctrlinfo_t));
	uint16_t mode = 0;
	if(!width && !height && !depth) {
		uint16_t *modes = (uint16_t*) VBE_PTR(vbe_ctrlinfo.modes);
		uint32_t modes_cnt = 0;
		while(modes[modes_cnt] != 0xFFFF) modes_cnt++;
		uint32_t disp_start = 0, pos = 0;
		text_clear();
		printf("Select video mode:\nPress UP/DOWN to choose the mode, then press ENTER/RETURN to select.\n\n");
		uint32_t start_y; text_getxy(NULL, &start_y);
		uint32_t disp_max = text_get_yres() - 1 - start_y;
		while(1) {
			for(uint32_t i = disp_start; i < (disp_start + disp_max) && i < modes_cnt; i++) {
				vbe_modeinfo_t *modeinfo = (vbe_modeinfo_t*) 0x3000;
				int32_regs_t regs;
				regs.ax = 0x4F01;
				regs.cx = modes[i];
				regs.es = 0x0000;
				regs.di = 0x3000;
				int32(0x10, &regs);
				printf("%c 0x%4x (%ux%ux%u)\n", (pos == i) ? '>' : ' ', modes[i], modeinfo->width, modeinfo->height, modeinfo->bpp);
			}
			uint32_t c = getc(NULL);
			if(c == KB_ARROW_UP && pos > 0) pos--;
			else if(c == KB_ARROW_DOWN && pos < (modes_cnt - 1)) pos++;
			else if(c == '\r') break;
			if(pos == disp_start && disp_start > 0) disp_start--;
			else if(pos == disp_start + disp_max) disp_start++;
			for(uint32_t i = disp_start; i < (disp_start + disp_max) && i < modes_cnt; i++) {
				text_setxy(0, start_y + i - disp_start); text_clrline();
			}
			text_setxy(0, start_y);
		}
		mode = modes[pos];
	} else mode = vbe_find_mode(width, height, depth);
	if(!mode) return -2; // cannot find matching video mode
	regs.ax = 0x4F02;
	regs.bx = mode | 0x4000; // enable LFB
	int32(0x10, &regs);
	regs.ax = 0x4F01;
	regs.cx = mode;
	regs.es = 0x0000;
	regs.di = 0x3000;
	int32(0x10, &regs);
	memcpy((void*) &vbe_modeinfo, (void*) 0x3000, sizeof(vbe_modeinfo_t));
	vbe_device.width = vbe_modeinfo.width; vbe_device.height = vbe_modeinfo.height; vbe_device.bpp = vbe_modeinfo.bpp;
	vbe_device.pitch = vbe_modeinfo.pitch;
	paging_map(vbe_modeinfo.physbase, 0xE0000000, vbe_device.height * vbe_device.pitch, 0, 1);
	vbe_device.buffer = (uint8_t*) (0xE0000000 + vbe_modeinfo.physbase - (vbe_modeinfo.physbase & ~0xFFF));
	vbe_device.color.endian = 0;
	switch(vbe_device.bpp) {
		case 15: vbe_device.color.r_start = 10;  vbe_device.color.r_bits = 5; vbe_device.color.g_start = 5; vbe_device.color.g_bits = 5; vbe_device.color.b_start = 0; vbe_device.color.b_bits = 5; vbe_device.color.bytes = 2; break;
		case 16: vbe_device.color.r_start = 11;  vbe_device.color.r_bits = 5; vbe_device.color.g_start = 5; vbe_device.color.g_bits = 6; vbe_device.color.b_start = 0; vbe_device.color.b_bits = 5; vbe_device.color.bytes = 2; break;
		case 24: vbe_device.color.r_start = 16;  vbe_device.color.r_bits = 8; vbe_device.color.g_start = 8; vbe_device.color.g_bits = 8; vbe_device.color.b_start = 0; vbe_device.color.b_bits = 8; vbe_device.color.bytes = 3; break;
		case 32: vbe_device.color.r_start = 16;  vbe_device.color.r_bits = 8; vbe_device.color.g_start = 8; vbe_device.color.g_bits = 8; vbe_device.color.b_start = 0; vbe_device.color.b_bits = 8; vbe_device.color.bytes = 4; break;
	}
	if(fb_default == NULL) fb_default = &vbe_device;
	return 0;
}
