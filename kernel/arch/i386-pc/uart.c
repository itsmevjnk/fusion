/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <uart.h>
#include <io.h>

const uint16_t uart_base[4] = {0x3f8, 0x2f8, 0x3e8, 0x2e8}; // base IO ports
const uint8_t uart_dbit[9] = {0, 0, 0, 0, 0, 0, 1, 2, 3}; // data bit mask
const uint8_t uart_sbit[3] = {0, 0, (1 << 2)}; // stop bit mask
const uint8_t uart_pbit[5] = {0, (1 << 3), ((1 << 3) | (1 << 4)), ((1 << 3) | (1 << 5)), ((1 << 3) | (1 << 4) | (1 << 5))}; // parity bit mask

void uart_init(uint8_t port, uint32_t baud, uint8_t dbit, uint8_t sbit, uint8_t parity) {
  baud = 115200 / baud;
  outb(uart_base[port] + 1, 0x00); // disable all interrupts
  outb(uart_base[port] + 3, 0x80); // enable DLAB
  outb(uart_base[port] + 0, (uint8_t) baud); // low byte of divisor
  outb(uart_base[port] + 1, (uint8_t) (baud >> 8)); // high byte of divisor
  outb(uart_base[port] + 3, (uart_dbit[dbit] | uart_sbit[sbit] | uart_pbit[parity]));
  outb(uart_base[port] + 2, 0xC7); // enable FIFO
  outb(uart_base[port] + 4, 0x03); // DTR+RTS enabled
}

void uart_putc(uint8_t port, uint8_t c) {
  while(!(inb(uart_base[port]) & 0x20));
  outb(uart_base[port], c);
}

uint8_t uart_getc(uint8_t port) {
  while(!(inb(uart_base[port] + 5) & 1));
  return inb(uart_base[port]);
}
