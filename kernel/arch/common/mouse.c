/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <mouse.h>
#include <framebuffer.h>

volatile uint32_t mouse_x = 0, mouse_y = 0;
volatile uint8_t mouse_buttons = 0;

void mouse_update(int32_t dx, int32_t dy) {
	int32_t x = mouse_x, y = mouse_y;
	if(x + dx < 0) x = 0;
	else if(x + dx >= (int32_t) fb_default->width) x = fb_default->width - 1;
	else x += dx;
	if(y + dy < 0) y = 0;
	else if(y + dy >= (int32_t) fb_default->height) y = fb_default->height - 1;
	else y += dy;
	mouse_x = x, mouse_y = y;
}
