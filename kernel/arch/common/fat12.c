/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fat12.h>
#include <vfs.h>
#include <bpb.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

bpb_t *bpb = NULL;
fat12_ebpb_t *ebpb = NULL;
vfs_node_t **fat12_nodes = NULL; // array of pointers to nodes that we've converted from directory entries
uint32_t fat12_entcnt = 0; // entry count
uint8_t **fat12_fat = NULL; // pointer to the FAT(s)
uint32_t fat12_sectors = 0; // total sectors

// magic values for storing in node's impl field
#define FAT12_IMPL_OPEN									0x210804FE
#define FAT12_IMPL_CLOSED								0x210804FF

/* convert sector to address */
inline uintptr_t fat12_sect2addr(uint32_t sector) {
	return ((uintptr_t) bpb + sector * bpb->bpsect);
}

/* convert address to sector */
inline uint32_t fat12_addr2sect(uintptr_t addr) {
	return ((addr - (uintptr_t) bpb) / bpb->bpsect);
}

/* convert address to sector offset */
inline uint32_t fat12_addr2soff(uintptr_t addr) {
	return ((addr - (uintptr_t) bpb) % bpb->bpsect);
}

/* convert cluster to sector */
inline uint32_t fat12_clus2sect(uint32_t cluster) {
	return (bpb->rsect + (bpb->fat * bpb->spfat) + ((bpb->rdent * sizeof(fat12_dirent_t)) + (bpb->bpsect - 1)) / bpb->bpsect + (cluster - 2) * bpb->spclus);
}

/* convert sector to cluster (UNTESTED) */
inline uint32_t fat12_sect2clus(uint32_t sector) {
	return (sector - ((bpb->rsect + (bpb->fat * bpb->spfat) + ((bpb->rdent * sizeof(fat12_dirent_t)) + (bpb->bpsect - 1)) / bpb->bpsect)) / bpb->spclus + 2);
}

/* convert directory entry to node */
vfs_node_t *fat12_ent2node(fat12_dirent_t *ent) {
	uintptr_t inode_match = (uintptr_t) ent;
	for(uint32_t i = 0; i < fat12_entcnt; i++) {
		if(fat12_nodes[i]->inode == inode_match) return fat12_nodes[i];
	}
	return NULL;
}

/* convert node to node index */
uint32_t fat12_node2idx(vfs_node_t *node) {
	for(uint32_t i = 0; i < fat12_entcnt; i++) {
		if(fat12_nodes[i] == node) return i;
	}
	return 0xFFFFFFFF;
}

/* acquire FAT data for next cluster */
inline uint16_t fat12_acqfat(uint8_t n, uint16_t cluster) {
	uint16_t ret = *(uint16_t*) &fat12_fat[n][cluster + cluster / 2];
	if(cluster & 1) ret >>= 4;
	else ret &= 0xFFF;
	return ret;
}

/* write FAT data for next cluster */
void fat12_wrfat(uint8_t n, uint16_t cluster, uint16_t dat) {
	uint16_t *t = (uint16_t*) &fat12_fat[n][cluster + cluster / 2];
	if(cluster & 1) *t = (*t & 0x000F) | (dat << 4);
	else *t = (*t & 0xF000) | (dat & 0xFFF);
}

/* write FAT data for next cluster, but on all FATs */
inline void fat12_glob_wrfat(uint16_t cluster, uint16_t dat) {
	for(uint8_t i = 0; i < bpb->fat; i++) fat12_wrfat(i, cluster, dat);
}

extern uint32_t fat12_vfs_read(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf); // done
extern uint32_t fat12_vfs_write(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf); // done
extern vfs_node_t *fat12_vfs_create(vfs_node_t *node, char *name);
extern vfs_node_t *fat12_vfs_mkdir(vfs_node_t *node, char *name);
extern int fat12_vfs_delete(vfs_node_t *node);
extern struct dirent *fat12_vfs_readdir(vfs_node_t *node, uint32_t index); // done
extern void fat12_vfs_open(vfs_node_t *node, uint8_t flags); // done
extern void fat12_vfs_close(vfs_node_t *node); // done
extern vfs_node_t *fat12_vfs_finddir(vfs_node_t *node, char *name); // done

void fat12_populate(vfs_node_t *node) {
	assert(node->flags == VFS_TYPE_DIR); // only accepts directory nodes as only they contain the directory entries
	fat12_dirent_t *dirent;
	fat12_dirent_t *de = (fat12_dirent_t*) node->inode;
	if(de->lclus == 0xFFFF) dirent = (fat12_dirent_t*) fat12_sect2addr(bpb->rsect + (bpb->fat * bpb->spfat));
	else dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(de->lclus));
	uint32_t mod = (bpb->bpsect * bpb->spclus) / sizeof(fat12_dirent_t);
	for(uint32_t i = 0; dirent[i % mod].name[0] != 0; i++) {
		if(node != vfs_root && i != 0 && i % mod == 0) {
			/* fetch next cluster of directory entry */
			uint16_t cluster = fat12_sect2clus(fat12_addr2sect((uintptr_t) dirent));
			uint16_t t = fat12_acqfat(0, cluster);
			if(t >= 0xFF8) break; // we've reached the end
			else if(t == 0xFF7) {
				/* bad cluster encountered, check for redundancies */
				for(uint8_t j = 1; j < bpb->fat; j++) {
					t = fat12_acqfat(j, cluster);
					if(t != 0xFF7) break;
				}
				if(t == 0xFF7) break; // no redundancy found
			} else cluster = t;
			dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(cluster));
		}
		if(dirent[i % mod].name[0] == 0xE5) continue;
		if(dirent[i % mod].attribs == FATTR_LFN) continue; // doesn't support LFN as of now
		if(dirent[i % mod].name[0] == '.' && (dirent[i % mod].name[1] == ' ' || dirent[i % mod].name[1] == '.') && (dirent[i % mod].attribs == FATTR_DIR)) continue; // "." or ".." detected, we don't need to parse them
		fat12_nodes = realloc(fat12_nodes, ++fat12_entcnt * sizeof(vfs_node_t*));
		fat12_nodes[fat12_entcnt - 1] = calloc(sizeof(vfs_node_t), 1);
		fat12_nodes[fat12_entcnt - 1]->flags = (dirent[i % mod].attribs == FATTR_DIR) ? VFS_TYPE_DIR : VFS_TYPE_FILE;
		for(uint8_t j = 0; j < 8 && dirent[i % mod].name[j] != ' '; j++)
			fat12_nodes[fat12_entcnt - 1]->name[j] = (dirent[i % mod].name[j] >= 'A' && dirent[i % mod].name[j] <= 'Z') ? (dirent[i % mod].name[j] + 32) : dirent[i % mod].name[j];
		if(fat12_nodes[fat12_entcnt - 1]->flags == VFS_TYPE_FILE) {
			fat12_nodes[fat12_entcnt - 1]->name[strlen(fat12_nodes[fat12_entcnt - 1]->name)] = '.';
			for(uint8_t j = 8; j < 11; j++)
				fat12_nodes[fat12_entcnt - 1]->name[strlen(fat12_nodes[fat12_entcnt - 1]->name)] = (dirent[i % mod].name[j] >= 'A' && dirent[i % mod].name[j] <= 'Z') ? (dirent[i % mod].name[j] + 32) : dirent[i % mod].name[j];
		}
		if(fat12_nodes[fat12_entcnt - 1]->flags == VFS_TYPE_FILE) {
			fat12_nodes[fat12_entcnt - 1]->len = dirent[i % mod].size;
			fat12_nodes[fat12_entcnt - 1]->read = (void*) fat12_vfs_read;
			fat12_nodes[fat12_entcnt - 1]->write = (void*) fat12_vfs_write;
			fat12_nodes[fat12_entcnt - 1]->delete = (void*) fat12_vfs_delete;
		} else {
			fat12_nodes[fat12_entcnt - 1]->readdir = (void*) fat12_vfs_readdir;
			fat12_nodes[fat12_entcnt - 1]->finddir = (void*) fat12_vfs_finddir;
			fat12_nodes[fat12_entcnt - 1]->create = (void*) fat12_vfs_create;
			fat12_nodes[fat12_entcnt - 1]->mkdir = (void*) fat12_vfs_mkdir;
			fat12_nodes[fat12_entcnt - 1]->rmdir = (void*) fat12_vfs_delete; // fat12_vfs_delete() can be used for deleting directories too (provided that the directory doesn't contain anything)
		}
		fat12_nodes[fat12_entcnt - 1]->open = (void*) fat12_vfs_open;
		fat12_nodes[fat12_entcnt - 1]->close = (void*) fat12_vfs_close;
		vfs_root->impl = FAT12_IMPL_CLOSED;
		fat12_nodes[fat12_entcnt - 1]->inode = (uintptr_t) &dirent[i % mod];
		// printf("%s %x %8x\n", fat12_nodes[fat12_entcnt - 1]->name, fat12_nodes[fat12_entcnt - 1]->flags, fat12_nodes[fat12_entcnt - 1]->inode);
		if(fat12_nodes[fat12_entcnt - 1]->flags == VFS_TYPE_DIR) fat12_populate(fat12_nodes[fat12_entcnt - 1]);
	}
}

void fat12_vfs_init(void *initrd) {
	bpb = (bpb_t*) initrd;
	ebpb = (fat12_ebpb_t*) &bpb->extra;
	fat12_sectors = (bpb->sect) ? bpb->sect : bpb->lsect;
	fat12_fat = malloc(sizeof(uint8_t*) * bpb->fat);
	for(uint32_t i = 0; i < bpb->fat; i++) fat12_fat[i] = (uint8_t*) fat12_sect2addr(bpb->rsect + i * bpb->spfat);
	vfs_root = calloc(sizeof(vfs_node_t), 1);
	vfs_root->flags = VFS_TYPE_DIR;
	fat12_dirent_t *root_dirent = calloc(sizeof(fat12_dirent_t), 1); // fake directory entry for root
	root_dirent->attribs = FATTR_DIR;
	root_dirent->lclus = 0xFFFF; // this signals to functions that it's the root directory, and its address would be fat12_sect2addr(bpb->rsect + (bpb->fat * bpb->spfat))
	vfs_root->inode = (uintptr_t) root_dirent;
	vfs_root->readdir = (void*) fat12_vfs_readdir;
	vfs_root->finddir = (void*) fat12_vfs_finddir;
	vfs_root->create = (void*) fat12_vfs_create;
	vfs_root->mkdir = (void*) fat12_vfs_mkdir;
	vfs_root->impl = FAT12_IMPL_OPEN; // always open
	fat12_populate(vfs_root); // populate the nodes list with root directory entries
	// printf("FAT12 nodes: %u\n", fat12_entcnt);
}

uint32_t fat12_vfs_read(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf) {
	if(node->impl != FAT12_IMPL_OPEN) return 0; // can only read if the file is opened
	if((offset + size - 1) > node->len) size -= offset + size - 1 - node->len;
	uint16_t clusno_begin = offset / (bpb->bpsect * bpb->spclus);
	uint16_t begin_off = offset % (bpb->bpsect * bpb->spclus);
	uint16_t clusno_end = (offset + size - 1) / (bpb->bpsect * bpb->spclus);
	uint16_t end_off = (offset + size - 1) % (bpb->bpsect * bpb->spclus);
	fat12_dirent_t *dirent = (fat12_dirent_t*) node->inode;
	uint16_t cluster = dirent->lclus;
	// printf("cluster #%u.%u->#%u.%u\n", clusno_begin, begin_off, clusno_end, end_off);
	/* first we need to skip to the beginning cluster */
	for(uint16_t i = 0; i < clusno_begin; i++) {
		uint16_t t = fat12_acqfat(0, cluster);
		if(t >= 0xFF8) return 0; // file ended too early
		else if(t == 0xFF7) {
			/* bad cluster encountered, check for redundancies */
			for(uint8_t j = 1; j < bpb->fat; j++) {
				t = fat12_acqfat(j, cluster);
				if(t != 0xFF7) break;
			}
			if(t == 0xFF7) return 0; // no redundancy found
		} else cluster = t;
	}
	/* now that cluster contains the first cluster to read, begin reading */
	uint32_t read = 0;
	for(uint16_t i = clusno_begin; i <= clusno_end; i++) {
		uint8_t *cdat = (uint8_t*) fat12_sect2addr(fat12_clus2sect(cluster));
		if(i == clusno_begin) {
			memcpy(buf, &cdat[begin_off], (clusno_begin == clusno_end) ? size : ((uint16_t) (bpb->bpsect * bpb->spclus - begin_off)));
			read += (clusno_begin == clusno_end) ? size : ((uint16_t) (bpb->bpsect * bpb->spclus - begin_off));
		} else if(i == clusno_end) {
			memcpy(&buf[read], cdat, end_off + 1);
			read += end_off + 1;
		} else {
			memcpy(&buf[read], cdat, bpb->bpsect * bpb->spclus);
			read += bpb->bpsect * bpb->spclus;
		}
		uint16_t t = fat12_acqfat(0, cluster);
		// printf("cluster #%u: %3x, next: %3x\n", i, cluster, t);
		if(t >= 0xFF8) return read;
		else if(t == 0xFF7) {
			/* bad cluster encountered, check for redundancies */
			for(uint8_t j = 1; j < bpb->fat; j++) {
				t = fat12_acqfat(j, cluster);
				if(t != 0xFF7) break;
			}
			if(t == 0xFF7) return read; // no redundancy found
		} else cluster = t;
	}
	return read;
}

/* scan for free cluster */
uint16_t fat12_free_cluster(void) {
	for(uint32_t i = 0; i < ((fat12_sectors - (bpb->rsect + bpb->fat * bpb->spfat + ((bpb->rdent * 32) + (bpb->bpsect - 1)) / bpb->bpsect)) / bpb->spclus); i++) {
		if(fat12_acqfat(0, i + 2) == 0) return (i + 2);
	}
	return 0xFFF;
}

uint32_t fat12_vfs_write(vfs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buf) {
	// printf("impl = 0x%x\n", node->impl);
	if(node->impl != FAT12_IMPL_OPEN) return 0; // can only write if the file is opened
	uint16_t clusno_begin = offset / (bpb->bpsect * bpb->spclus);
	uint16_t begin_off = offset % (bpb->bpsect * bpb->spclus);
	uint16_t clusno_end = (offset + size - 1) / (bpb->bpsect * bpb->spclus);
	uint16_t end_off = (offset + size - 1) % (bpb->bpsect * bpb->spclus);
	uint16_t new_clus = clusno_end + ((end_off) ? 1 : 0);
	uint16_t curr_clus = (node->len / (bpb->bpsect * bpb->spclus)) + (node->len % (bpb->bpsect * bpb->spclus)) ? 1 : 0;
	fat12_dirent_t *dirent = (fat12_dirent_t*) node->inode;
	uint16_t cluster = dirent->lclus;
	// printf("new_clus = %u curr_clus = %u\n", new_clus, curr_clus);
	if(new_clus > curr_clus) {
		/* we need to grow the file by adding more clusters */
		/* first, traverse to the end of the cluster list */
		uint16_t clus2 = 0;
		if(cluster != 0) {
			/* only traverse if there's already a cluster */
			clus2	= cluster;
		 	while(1) {
			 	uint16_t t = fat12_acqfat(0, clus2);
			 	if(t >= 0xFF8) break; // we've reached the end
			 	else if(t == 0xFF7) {
					/* bad cluster encountered */
				 	for(uint8_t i = 0; i < bpb->fat; i++) {
					 	t = fat12_acqfat(i, cluster);
					 	if(t != 0xFF7) break;
				 	}
			 		if(t == 0xFF7) return 0; // this file has a bad cluster, and we can't do anything about it (yet) so we're going to abort
		 		} else clus2 = t;
			}
		}
		/* at this point, clus2 will contain the last cluster (if there's already a cluster), and its corresponding data in the FAT is >= 0xFF8 */
		for(uint16_t i = 0; i < (new_clus - curr_clus); i++) {
			uint16_t t = fat12_free_cluster();
			if(t == 0xFFF) return 0; // can't write anything because there's not enough space left
			memset((void*) fat12_sect2addr(fat12_clus2sect(t)), 0, bpb->spclus * bpb->bpsect); // wipe the cluster clean
			if(clus2) fat12_glob_wrfat(clus2, t);
			else {
				dirent->lclus = t;
				cluster = t;
			}
			fat12_glob_wrfat(t, 0xFFF); // so that we have to do less cleaning up
			clus2 = t; // next!
		}
	}
	node->len = (offset + size > node->len) ? (offset + size) : node->len;
	dirent->size = (offset + size > dirent->size) ? (offset + size) : dirent->size;
	/* first we need to skip to the beginning cluster */
	for(uint16_t i = 0; i < clusno_begin; i++) {
		uint16_t t = fat12_acqfat(0, cluster);
		if(t >= 0xFF8) return 0; // file ended too early
		else if(t == 0xFF7) {
			/* bad cluster encountered, check for redundancies */
			for(uint8_t j = 1; j < bpb->fat; j++) {
				t = fat12_acqfat(j, cluster);
				if(t != 0xFF7) break;
			}
			if(t == 0xFF7) return 0; // no redundancy found
		} else cluster = t;
	}
	/* now that cluster contains the first cluster to write into, start writing */
	uint32_t written = 0;
	for(uint16_t i = clusno_begin; i <= clusno_end; i++) {
		uint8_t *cdat = (uint8_t*) fat12_sect2addr(fat12_clus2sect(cluster));
		if(i == clusno_begin) {
			memcpy(&cdat[begin_off], buf, (clusno_begin == clusno_end) ? size : ((uint16_t) (bpb->bpsect * bpb->spclus - begin_off)));
			written += (clusno_begin == clusno_end) ? size : ((uint16_t) (bpb->bpsect * bpb->spclus - begin_off));
		} else if(i == clusno_end) {
			memcpy(cdat, &buf[written], end_off + 1);
			written += end_off + 1;
		} else {
			memcpy(cdat, &buf[written], bpb->bpsect * bpb->spclus);
			written += bpb->bpsect * bpb->spclus;
		}
		uint16_t t = fat12_acqfat(0, cluster);
		// printf("cluster #%u: %3x, next: %3x\n", i, cluster, t);
		if(t >= 0xFF8) return written;
		else if(t == 0xFF7) {
			/* bad cluster encountered, check for redundancies */
			for(uint8_t j = 1; j < bpb->fat; j++) {
				t = fat12_acqfat(j, cluster);
				if(t != 0xFF7) break;
			}
			if(t == 0xFF7) return written; // no redundancy found
		} else cluster = t;
	}
	return written;
	return 0;
}

struct dirent *fat12_vfs_readdir(vfs_node_t *node, uint32_t index) {
	uint32_t mod = (bpb->bpsect * bpb->spclus) / sizeof(fat12_dirent_t);
	fat12_dirent_t *dirent;
	fat12_dirent_t *de = (fat12_dirent_t*) node->inode;
	if(de->lclus == 0xFFFF) dirent = (fat12_dirent_t*) fat12_sect2addr(bpb->rsect + (bpb->fat * bpb->spfat));
	else dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(de->lclus));
	// printf("dirent = 0x%8x (first entry: %11s) index = %u\n", dirent, dirent->name, index);
	uint32_t x = 0, i;
	for(i = 0; x <= index; i++) {
		if(node != vfs_root && i != 0 && i % mod == 0) {
			/* fetch next cluster of directory entry */
			uint16_t cluster = fat12_sect2clus(fat12_addr2sect((uintptr_t) dirent));
			uint16_t t = fat12_acqfat(0, cluster);
			if(t >= 0xFF8) break; // we've reached the end
			else if(t == 0xFF7) {
				/* bad cluster encountered, check for redundancies */
				for(uint8_t j = 1; j < bpb->fat; j++) {
					t = fat12_acqfat(j, cluster);
					if(t != 0xFF7) break;
				}
				if(t == 0xFF7) break; // no redundancy found
			} else cluster = t;
			dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(cluster));
		}
		if(dirent[i % mod].name[0] == 0) return NULL;
		if(dirent[i % mod].name[0] == 0xE5) continue;
		if(dirent[i % mod].attribs == FATTR_LFN) continue; // doesn't support LFN as of now
		if(dirent[i % mod].name[0] == '.' && (dirent[i % mod].name[1] == ' ' || dirent[i % mod].name[1] == '.') && (dirent[i % mod].attribs == FATTR_DIR)) continue; // "." or ".." detected, we don't need to parse them
		x++;
	}
	x--;
	struct dirent *ret = calloc(sizeof(struct dirent), 1);
	// printf("%s\n", &dirent[(i - 1) % mod]);
	vfs_node_t *t = fat12_ent2node(&dirent[(i - 1) % mod]);
	// printf("found %x (%s)\n", t, t->name);
	strcpy(ret->name, t->name);
	ret->inode = t->inode;
	return ret;
}

void fat12_vfs_open(vfs_node_t *node, uint8_t flags) {
	(void) flags;
	node->impl = FAT12_IMPL_OPEN;
}

void fat12_vfs_close(vfs_node_t *node) {
	node->impl = FAT12_IMPL_CLOSED;
}

vfs_node_t *fat12_vfs_finddir(vfs_node_t *node, char *name) {
	fat12_dirent_t *dirent;
	fat12_dirent_t *de = (fat12_dirent_t*) node->inode;
	if(de->lclus == 0xFFFF) dirent = (fat12_dirent_t*) fat12_sect2addr(bpb->rsect + (bpb->fat * bpb->spfat));
	else dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(de->lclus));
	uint32_t mod = (bpb->bpsect * bpb->spclus) / sizeof(fat12_dirent_t);
	for(uint32_t i = 0; dirent[i % mod].name[0] != 0; i++) {
		if(node != vfs_root && i != 0 && i % mod == 0) {
			/* fetch next cluster of directory entry */
			uint16_t cluster = fat12_sect2clus(fat12_addr2sect((uintptr_t) dirent));
			uint16_t t = fat12_acqfat(0, cluster);
			if(t >= 0xFF8) break; // we've reached the end
			else if(t == 0xFF7) {
				/* bad cluster encountered, check for redundancies */
				for(uint8_t j = 1; j < bpb->fat; j++) {
					t = fat12_acqfat(j, cluster);
					if(t != 0xFF7) break;
				}
				if(t == 0xFF7) break; // no redundancy found
			} else cluster = t;
			dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(cluster));
		}
		if(dirent[i % mod].name[0] == 0xE5) continue;
		if(dirent[i % mod].attribs == FATTR_LFN) continue; // doesn't support LFN as of now
		if(dirent[i % mod].name[0] == '.' && (dirent[i % mod].name[1] == ' ' || dirent[i % mod].name[1] == '.') && (dirent[i % mod].attribs == FATTR_DIR)) continue; // "." or ".." detected, we don't need to parse them
		vfs_node_t *ret = fat12_ent2node(&dirent[i % mod]);
		if(!strcmp(ret->name, name)) return ret;
	}
	return NULL;
}

vfs_node_t *fat12_vfs_create(vfs_node_t *node, char *name) {
	fat12_dirent_t *dirent;
	fat12_dirent_t *de = (fat12_dirent_t*) node->inode;
	if(de->lclus == 0xFFFF) dirent = (fat12_dirent_t*) fat12_sect2addr(bpb->rsect + (bpb->fat * bpb->spfat));
	else dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(de->lclus));
	uint32_t mod = (bpb->bpsect * bpb->spclus) / sizeof(fat12_dirent_t);
	uint32_t i;
	for(i = 0; dirent[i % mod].name[0] != 0; i++) {
		if(node != vfs_root && i != 0 && i % mod == 0) {
			/* fetch next cluster of directory entry */
			uint16_t cluster = fat12_sect2clus(fat12_addr2sect((uintptr_t) dirent));
			uint16_t t = fat12_acqfat(0, cluster);
			if(t >= 0xFF8) {
				/* we've reached the end */
				uint16_t nclus = fat12_free_cluster();
				if(nclus == 0xFFF) return NULL; // can't write anything because there's not enough space left
				memset((void*) fat12_sect2addr(fat12_clus2sect(nclus)), 0, bpb->spclus * bpb->bpsect); // wipe the cluster clean
				fat12_glob_wrfat(cluster, nclus);
				fat12_glob_wrfat(nclus, 0xFFF); // so that we have to do less cleaning up
				cluster = nclus;
				dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(cluster));
				break;
			} else if(t == 0xFF7) {
				/* bad cluster encountered, check for redundancies */
				for(uint8_t j = 1; j < bpb->fat; j++) {
					t = fat12_acqfat(j, cluster);
					if(t != 0xFF7) break;
				}
				if(t == 0xFF7) break; // no redundancy found
			} else cluster = t;
			dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(cluster));
		}
		if(dirent[i % mod].name[0] == 0xE5) break;
	}
	/* after this, i stores an empty slot in the directory entry table */
	i %= mod;
	memset(dirent[i].name, ' ', 11);
	uint8_t j = 0;
	for(j = 0; j < 8 && name[j] != '.'; j++) dirent[i].name[j] = (name[j] >= 'a' && name[j] <= 'z') ? (name[j] - 32) : name[j];
	for(; j < 11 && name[j] != '.'; j++) {}
	j++; uint8_t j2 = j;
	for(; j < 11 && name[j] != 0; j++) dirent[i].name[8 + (j - j2)] = (name[j] >= 'a' && name[j] <= 'z') ? (name[j] - 32) : name[j];
	dirent[i].attribs = FATTR_ARCH; // Windows sets the attributes to 0x20
	fat12_nodes = realloc(fat12_nodes, ++fat12_entcnt * sizeof(vfs_node_t*));
	fat12_nodes[fat12_entcnt - 1] = calloc(sizeof(vfs_node_t), 1);
	strcpy(fat12_nodes[fat12_entcnt - 1]->name, name);
	fat12_nodes[fat12_entcnt - 1]->len = 0;
	fat12_nodes[fat12_entcnt - 1]->read = (void*) fat12_vfs_read;
	fat12_nodes[fat12_entcnt - 1]->write = (void*) fat12_vfs_write;
	fat12_nodes[fat12_entcnt - 1]->delete = (void*) fat12_vfs_delete;
	fat12_nodes[fat12_entcnt - 1]->flags = VFS_TYPE_FILE;
	fat12_nodes[fat12_entcnt - 1]->impl = FAT12_IMPL_CLOSED;
	fat12_nodes[fat12_entcnt - 1]->inode = (uintptr_t) &dirent[i];
	fat12_nodes[fat12_entcnt - 1]->open = (void*) fat12_vfs_open;
	fat12_nodes[fat12_entcnt - 1]->close = (void*) fat12_vfs_close;
	return fat12_nodes[fat12_entcnt - 1];
}

vfs_node_t *fat12_vfs_mkdir(vfs_node_t *node, char *name) {
	uint16_t nclus = fat12_free_cluster();
	if(nclus == 0xFFF) return NULL;
	memset((void*) fat12_sect2addr(fat12_clus2sect(nclus)), 0, bpb->spclus * bpb->bpsect); // wipe the cluster clean
	fat12_glob_wrfat(nclus, 0xFFF);
	fat12_dirent_t *dirent;
	fat12_dirent_t *de = (fat12_dirent_t*) node->inode;
	if(de->lclus == 0xFFFF) dirent = (fat12_dirent_t*) fat12_sect2addr(bpb->rsect + (bpb->fat * bpb->spfat));
	else dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(de->lclus));
	uint32_t mod = (bpb->bpsect * bpb->spclus) / sizeof(fat12_dirent_t);
	uint32_t i;
	for(i = 0; dirent[i % mod].name[0] != 0; i++) {
		if(node != vfs_root && i != 0 && i % mod == 0) {
			/* fetch next cluster of directory entry */
			uint16_t cluster = fat12_sect2clus(fat12_addr2sect((uintptr_t) dirent));
			uint16_t t = fat12_acqfat(0, cluster);
			if(t >= 0xFF8) {
				/* we've reached the end */
				uint16_t nclus = fat12_free_cluster();
				if(nclus == 0xFFF) return NULL; // can't write anything because there's not enough space left
				memset((void*) fat12_sect2addr(fat12_clus2sect(nclus)), 0, bpb->spclus * bpb->bpsect); // wipe the cluster clean
				fat12_glob_wrfat(cluster, nclus);
				fat12_glob_wrfat(nclus, 0xFFF); // so that we have to do less cleaning up
				cluster = nclus;
				dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(cluster));
				break;
			} else if(t == 0xFF7) {
				/* bad cluster encountered, check for redundancies */
				for(uint8_t j = 1; j < bpb->fat; j++) {
					t = fat12_acqfat(j, cluster);
					if(t != 0xFF7) break;
				}
				if(t == 0xFF7) break; // no redundancy found
			} else cluster = t;
			dirent = (fat12_dirent_t*) fat12_sect2addr(fat12_clus2sect(cluster));
		}
		if(dirent[i % mod].name[0] == 0xE5) break;
	}
	/* after this, i stores an empty slot in the directory entry table */
	i %= mod;
	memset(dirent[i].name, ' ', 11);
	uint8_t j = 0;
	for(j = 0; j < 8; j++) dirent[i].name[j] = (name[j] >= 'a' && name[j] <= 'z') ? (name[j] - 32) : name[j];
	dirent[i].attribs = FATTR_DIR;
	dirent[i].lclus = nclus;
	fat12_nodes = realloc(fat12_nodes, ++fat12_entcnt * sizeof(vfs_node_t*));
	fat12_nodes[fat12_entcnt - 1] = calloc(sizeof(vfs_node_t), 1);
	strcpy(fat12_nodes[fat12_entcnt - 1]->name, name);
	fat12_nodes[fat12_entcnt - 1]->len = 0;
	fat12_nodes[fat12_entcnt - 1]->flags = VFS_TYPE_DIR;
	fat12_nodes[fat12_entcnt - 1]->impl = FAT12_IMPL_CLOSED;
	fat12_nodes[fat12_entcnt - 1]->inode = (uintptr_t) &dirent[i];
	fat12_nodes[fat12_entcnt - 1]->readdir = (void*) fat12_vfs_readdir;
	fat12_nodes[fat12_entcnt - 1]->finddir = (void*) fat12_vfs_finddir;
	fat12_nodes[fat12_entcnt - 1]->create = (void*) fat12_vfs_create;
	fat12_nodes[fat12_entcnt - 1]->mkdir = (void*) fat12_vfs_mkdir;
	fat12_nodes[fat12_entcnt - 1]->rmdir = (void*) fat12_vfs_delete; // fat12_vfs_delete() can be used for deleting directories too (provided that the directory doesn't contain anything)
	fat12_nodes[fat12_entcnt - 1]->open = (void*) fat12_vfs_open;
	fat12_nodes[fat12_entcnt - 1]->close = (void*) fat12_vfs_close;
	return fat12_nodes[fat12_entcnt - 1];
}

int fat12_vfs_delete(vfs_node_t *node) {
	if(node->impl == FAT12_IMPL_OPEN) return -1; // file still open
	fat12_dirent_t *dirent = (fat12_dirent_t*) node->inode;
	uint16_t cluster = dirent->lclus;
	if(cluster) {
		/* invalidate the cluster chain */
		while(1) {
			uint16_t t = fat12_acqfat(0, cluster);
			if(t == 0xFF7) {
				/* bad cluster encountered, check for redundancies */
				for(uint8_t j = 1; j < bpb->fat; j++) {
					t = fat12_acqfat(j, cluster);
					if(t != 0xFF7) break;
				}
			}
			fat12_glob_wrfat(cluster, 0);
			if(t >= 0xFF7) break; // that's the end
			else cluster = t;
		}
	}
	dirent->name[0] = 0xE5; // invalidate the directory entry (can be undeleted)
	free(node); // deallocate the node
	/* shrink the nodes list */
	uint32_t idx = fat12_node2idx(node);
	assert(idx < fat12_entcnt);
	for(uint32_t i = idx + 1; i < fat12_entcnt; i++) fat12_nodes[i - 1] = fat12_nodes[i];
	fat12_nodes = realloc(fat12_nodes, --fat12_entcnt * sizeof(vfs_node_t*));
	return 0;
}
