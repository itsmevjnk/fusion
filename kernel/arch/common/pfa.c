/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <pfa.h>
#include <placement.h>
#include <string.h>

uintptr_t pfa_size = 0;
uintptr_t *pfa_bitmap = NULL;

void pfa_setframe(uintptr_t frame) {
	if(frame >= pfa_size) return;
	pfa_bitmap[frame / 32] |= (1 << (frame % 32));
}

void pfa_clrframe(uintptr_t frame) {
	if(frame >= pfa_size) return;
	pfa_bitmap[frame / 32] &= ~(1 << (frame % 32));
}

uint8_t pfa_statframe(uintptr_t frame) {
	if(frame >= pfa_size) return 1;
	return (pfa_bitmap[frame / 32] & (1 << (frame % 32))) ? 1 : 0;
}

void pfa_reserve(uintptr_t addr, uintptr_t size) {
	uintptr_t addr_aligned = addr & ~0xFFF;
	size += addr - addr_aligned;
	for(uintptr_t i = 0; i < size && ((addr_aligned + i) >> 12) < pfa_size; i += 0x1000) pfa_setframe((addr_aligned + i) >> 12);
}

void pfa_free(uintptr_t addr, uintptr_t size) {
	uintptr_t addr_aligned = addr & ~0xFFF;
	size += addr - addr_aligned;
	for(uintptr_t i = 0; i < size && ((addr_aligned + i) >> 12) < pfa_size; i += 0x1000) pfa_clrframe((addr_aligned + i) >> 12);
}

uintptr_t pfa_freeframe(void) {
	for(uintptr_t i = 0; i < pfa_size / (sizeof(uintptr_t) * 8); i++) {
		for(uint8_t j = 0; j < sizeof(uintptr_t) * 8; j++) {
			if(!(pfa_bitmap[i] & (1 << j))) return (i * sizeof(uintptr_t) * 8 + j);
		}
	}
	return UINTPTR_MAX; // no available frame
}

void pfa_init(uintptr_t size) {
	pfa_size = size / 32768;
	pfa_bitmap = (uintptr_t*) pmalloc(size / 32768, 0); // 4096 * 8 = 32768
	memset(pfa_bitmap, 0, size / 32768);
}
