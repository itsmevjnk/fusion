/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <devfs.h>
#include <vfs.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

vfs_node_t *devfs_root = NULL;

/* linked list of nodes */
typedef struct devfs_node {
	vfs_node_t node;
	struct devfs_node *next;
} __attribute__ ((packed)) devfs_node_t;
devfs_node_t *devfs_node = NULL;

struct dirent *devfs_vfs_readdir(vfs_node_t *node, uint32_t index) {
	(void) node; // no one cares(tm)
	devfs_node_t *entry = devfs_node;
	for(uint32_t i = 0; entry != NULL && i < index; i++) {
		entry = (devfs_node_t*) entry->next;
	}
	if(entry == NULL) return NULL;
	struct dirent *ret = malloc(sizeof(struct dirent));
	strcpy(ret->name, entry->node.name);
	ret->inode = entry->node.inode;
	return ret;
}

void devfs_vfs_open(vfs_node_t *node, uint8_t flags) {
	(void) flags;
	node->impl = DEVFS_IMPL_OPEN;
}

void devfs_vfs_close(vfs_node_t *node) {
	node->impl = DEVFS_IMPL_CLOSED;
}

vfs_node_t *devfs_vfs_finddir(vfs_node_t *node, char *name) {
	(void) node; // no one cares(tm)
	devfs_node_t *entry = devfs_node;
	while(entry != NULL && strcmp(entry->node.name, name) != 0) {
		entry = (devfs_node_t*) entry->next;
	}
	if(entry == NULL) return NULL;
	return &entry->node;
}

vfs_node_t *devfs_register(char *name, vfs_read_t read, vfs_write_t write) {
	devfs_node_t *node = calloc(sizeof(devfs_node_t), 1);
	strcpy(node->node.name, name);
	node->node.inode = (uintptr_t) node;
	node->node.flags = VFS_TYPE_FILE;
	node->node.impl = DEVFS_IMPL_CLOSED;
	node->node.open = (void*) devfs_vfs_open;
	node->node.close = (void*) devfs_vfs_close;
	node->node.read = read;
	node->node.write = write;
	if(devfs_node == NULL) devfs_node = node;
	else {
		devfs_node_t *entry = devfs_node;
		while(entry->next != NULL) {
			entry = (devfs_node_t*) entry->next;
		}
		entry->next = (struct devfs_node*) node;
	}
	// printf("created %s with read 0x%x and write 0x%x\n", node->node.name, node->node.read, node->node.write);
	return &node->node;
}

int devfs_unregister(vfs_node_t *device) {
	if(device->impl == DEVFS_IMPL_OPEN) return -1; // device still in use
	devfs_node_t *node = (devfs_node_t*) device->inode;
	devfs_node_t *node_before = devfs_node;
	while(node_before->next != node) {
		node_before = (devfs_node_t*) node_before->next;
	}
	node_before->next = node->next;
	free(node);
	return 0;
}

uint8_t devfs_isopen(vfs_node_t *device) {
	return ((device->impl == DEVFS_IMPL_OPEN) ? 1 : 0);
}

int devfs_init(void) {
	devfs_root = vfs_mkdir(vfs_root, "dev");
	if(devfs_root == NULL) return -1;
	devfs_root->impl = DEVFS_IMPL_OPEN;
	devfs_root->open = NULL;
	devfs_root->close = NULL;
	devfs_root->read = NULL;
	devfs_root->write = NULL;
	devfs_root->readdir = (void*) devfs_vfs_readdir;
	devfs_root->finddir = (void*) devfs_vfs_finddir;
	devfs_root->create = NULL;
	devfs_root->delete = NULL;
	devfs_root->mkdir = NULL;
	devfs_root->rmdir = NULL;
	devfs_root->ptr = NULL;
	return 0;
}
