/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <psf.h>
#include <utf8.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <framebuffer.h>
#include <textmode.h>

#define UNICODE_MAX									137374 // remember to update this if there's a new Unicode character

uint32_t *psf_uctab = NULL; // Unicode table
uint32_t psf_width = 0, psf_height = 0, psf_gsize = 0, psf_gcnt = 0, psf_lsize = 0;
uint8_t *psf_glyphs = NULL; // glyphs

void psf_init(void* file, uint32_t size) {
	uint32_t *magic = (uint32_t*) file;
	psf_uctab = calloc(UNICODE_MAX, sizeof(uint32_t*));
	if((*magic & 0xFFFF) == PSF1_MAGIC) {
		/* PSF1 */
		psf1_t *psf = (psf1_t*) file;
		psf_width = 8;
		psf_height = psf->height;
		psf_gsize = psf_height;
		psf_gcnt = (psf->mode & PSF1_MODE_512) ? 512 : 256;
		psf_glyphs = malloc(psf_gcnt * psf_gsize);
		memcpy(psf_glyphs, (void*) ((uintptr_t) file + sizeof(psf1_t)), psf_gcnt * psf_gsize);
		if(psf->mode & PSF1_MODE_TAB) {
			/* parse Unicode table */
			uint16_t *uctab = (uint16_t*) ((uintptr_t) file + sizeof(psf1_t) + psf_gcnt * psf_gsize);
			uint32_t uctab_i = 0;
			for(uint16_t i = 0; i < psf_gcnt && ((uintptr_t) uctab + uctab_i * 2) < ((uintptr_t) file + size); i++) {
				for(; uctab[uctab_i] != 0xFFFF; uctab_i++) psf_uctab[uctab[uctab_i]] = i;
				uctab_i++; // skip 0xFFFF
			}
		} else for(uint32_t i = 0; i < psf_gcnt; i++) psf_uctab[i] = i;
	} else if(*magic == PSF2_MAGIC) {
		/* PSF2 */
		psf2_t *psf = (psf2_t*) file;
		psf_width = psf->width; psf_height = psf->height;
		psf_gsize = psf->gsize; psf_gcnt = psf->glyphs;
		psf_glyphs = malloc(psf_gsize * psf_gcnt);
		memcpy(psf_glyphs, (void*) ((uintptr_t) file + psf->hdr_size), psf_gcnt * psf_gsize);
		if(psf->flags & PSF2_FLAGS_UCTAB) {
			/* parse Unicode table */
			uint8_t *uctab = (uint8_t*) ((uintptr_t) file + psf->hdr_size + psf_gcnt * psf_gsize);
			for(uint32_t i = 0; i < psf_gcnt && ((uintptr_t) uctab) < ((uintptr_t) file + size); i++) {
				while(*uctab != 0xFF) {
					uint32_t c = utf8_translate_32(uctab);
					uctab += utf8_bytes32(c);
					psf_uctab[c] = i;
				}
				uctab++;
			}
		} else for(uint32_t i = 0; i < psf_gcnt; i++) psf_uctab[i] = i;
	}
	psf_lsize = (psf_width + 7) / 8;
}

void psf_writec(uint32_t c, uint32_t x, uint32_t y, uint32_t bg, uint32_t fg) {
	assert(psf_glyphs != NULL);
	c = psf_uctab[c];
	for(uint32_t i = 0; i < psf_height; i++) {
		for(uint32_t j = 0; j < psf_width; j++) {
			fb_putpixel(text_fb, x + j, y + i, (psf_glyphs[c * psf_gsize + i * psf_lsize + j / 8] & (1 << (7 - (j % 8)))) ? fg : bg);
		}
	}
}
