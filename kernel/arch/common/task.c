/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <task.h>

uintptr_t task_pidmap[TASK_MAXPROCS];

uint32_t task_pid_alloc(uintptr_t task) {
	for(uint32_t i = 1; i < TASK_MAXPROCS; i++) {
		if(!task_pidmap[i]) {
			task_pidmap[i] = task;
			return i;
		}
	}
	return 0;
}

void task_pid_free(uint32_t pid) {
	task_pidmap[pid] = 0;
}

uintptr_t task_pid2task(uint32_t pid) {
	return task_pidmap[pid];
}

uint32_t task_task2pid(uintptr_t task) {
	for(uint32_t i = 0; i < TASK_MAXPROCS; i++) {
		if(task_pidmap[i] == task) return i;
	}
	return 0;
}

uint32_t task_getpid(void) {
	return task_task2pid(task_running);
}

void task_switch_pid(uint32_t pid) {
	task_switch(task_pid2task(pid));
}
