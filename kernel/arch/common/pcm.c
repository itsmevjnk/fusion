/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <pcm.h>

uint32_t pcm_load(pcm_device_t *device, pcm_frame_t *frame, uint32_t n) {
	if(device->load != NULL) return device->load((void*) device, frame, n);
	return 0;
}

void pcm_play(pcm_device_t *device) {
	if(device->play != NULL) pcm_play((void*) device);
}

void pcm_stop(pcm_device_t *device) {
	if(device->stop != NULL) pcm_stop((void*) device);
}