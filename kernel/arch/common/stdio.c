/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <textmode.h>
#include <utf8.h>
#include <keyboard.h>
#include <vfs.h>
#include <tty.h>
#include <devfs.h>

/* NEW: standard streams */
vfs_node_t *stdin = NULL;
vfs_node_t *stdout = NULL;

int stdio_vfs_init(void) {
	if(stdin != NULL && stdout != NULL) return 0;
	stdin = devfs_register("stdin", (void*) tty_vfs_read, NULL);
	stdout = devfs_register("stdout", NULL, (void*) tty_vfs_write);
	return 0; // success
}

int putchar(uint32_t c) {
	if(stdout == NULL) text_putc(c);
	else {
		vfs_open(stdout, 0);
		uint8_t utf8[8];
		vfs_write(stdout, 0, utf8_translate_8(c, (uint8_t*) &utf8), (uint8_t*) &utf8);
		vfs_close(stdout);
	}
	return c;
}

static void print(char *s) {
	if(stdout == NULL) {
		size_t i = 0;
		while(i < strlen(s)) {
			uint32_t c = utf8_translate_32((uint8_t*) &s[i]);
			text_putc(c);
			i += utf8_bytes(s[i]);
		}
	} else {
		vfs_open(stdout, 0);
		vfs_write(stdout, 0, strlen(s), (uint8_t*) s);
		vfs_close(stdout);
	}
}

int vsprintf(char *str, const char *format, va_list params) {
	uint32_t i = 0, j = 0;
	while(format[i] != '\0') {
		if(format[i] == '%') {
			i++;

			uint32_t len = 0;
			if(format[i] >= '0' && format[i] <= '9') {
				char len_arr[12];
				uint32_t t = 0;
				while(format[i] >= '0' && format[i] <= '9') {
					len_arr[t++] = format[i++];
				}
				len_arr[t] = '\0';
				len = atoi(len_arr);
			} else if(format[i] == '*') {
				i++;
				len = (uint32_t) va_arg(params, int);
			}

			char flag = format[i++];

			if(flag == 'c') {
				char c = (char) va_arg(params, int);
				str[j++] = c;
			} else if(flag == 's') {
				const char *s = va_arg(params, const char *);
				for(size_t t = 0; t < ((len == 0) ? strlen(s) : len); t++) {
					str[j++] = s[t];
				}
			} else if(flag == '%') {
				str[j++] = '%';
			} else if(flag == 'n') {
				uint32_t *ptr = va_arg(params, uint32_t *);
				*ptr = j;
			} else if(flag == 'u') {
				uint32_t a = va_arg(params, uint32_t);
				uint32_t skip = 0;
				char num[100];
				itoa_unsigned(a, num, 10);
				if(strlen(num) < len) skip = len - strlen(num);
				for(uint32_t t = 0; t < skip; t++) num[t] = '0';
				itoa_unsigned(a, num + skip, 10);
				for(size_t t = 0; t < strlen(num); t++) {
					str[j++] = num[t];
				}
			} else if(flag == 'd' || flag == 'i') {
				int a = va_arg(params, int);
				char num[100];
				itoa(a, num, 10);
				for(size_t t = 0; t < strlen(num); t++) {
					str[j++] = num[t];
				}
			} else if(flag == 'x') {
				uint32_t a = va_arg(params, uint32_t);
				uint32_t skip = 0;
				char num[100];
				itoa_unsigned(a, num, 16);
				if(strlen(num) < len) skip = len - strlen(num);
				for(uint32_t t = 0; t < skip; t++) num[t] = '0';
				itoa_unsigned(a, num + skip, 16);
				for(size_t t = 0; t < strlen(num); t++) {
					str[j++] = num[t];
				}
			} else if(flag == 'X') {
				uint32_t a = va_arg(params, uint32_t);
				uint32_t skip = 0;
				char num[100];
				itoa_unsigned(a, num, 16);
				if(strlen(num) < len) skip = len - strlen(num);
				for(uint32_t t = 0; t < skip; t++) num[t] = '0';
				itoa_unsigned(a, num + skip, 16);
				for(size_t t = 0; t < strlen(num); t++) {
					if(num[t] >= 'a') num[t] = num[t] - 'a' + 'A';
					str[j++] = num[t];
				}
			} else if(flag == 'p') {
				uint32_t *a = va_arg(params, uint32_t *);
				uint32_t skip = 0;
				char num[100];
				itoa_unsigned(*a, num, 16);
				if(strlen(num) < len) skip = len - strlen(num);
				for(uint32_t t = 0; t < skip; t++) num[t] = '0';
				itoa_unsigned(*a, num + skip, 16);
				for(size_t t = 0; t < strlen(num); t++) {
					str[j++] = num[t];
				}
			} else if(flag == 'o') {
				uint32_t a = va_arg(params, uint32_t);
				uint32_t skip = 0;
				char num[100];
				itoa_unsigned(a, num, 8);
				if(strlen(num) < len) skip = len - strlen(num);
				for(uint32_t t = 0; t < skip; t++) num[t] = '0';
				itoa_unsigned(a, num + skip, 8);
				for(size_t t = 0; t < strlen(num); t++) {
					str[j++] = num[t];
				}
			}
		} else {
			str[j++] = format[i++];
		}
	}
	str[j] = 0;
	return j;
}

int sprintf(char *str, const char *format, ...) {
	va_list params;
	va_start(params, format);
	int ret = vsprintf(str, format, params);
	va_end(params);
	return ret;
}

int printf(const char *format, ...) {
	va_list params;
	va_start(params, format);
	char str[4096];
	int ret = vsprintf(str, format, params);
	print(str);
	va_end(params);
	return ret;
}

int puts(const char *str) {
	return printf("%s\n", str);
}

uint32_t getc(vfs_node_t *stream) {
	uint32_t c = 0;
	if(stream != NULL) {
		if(stream == stdin) vfs_open(stdin, 0);
		uint8_t buf[8];
		vfs_read(stream, 0, 0, (uint8_t*) &buf);
		c = utf8_translate_32((uint8_t*) &buf);
		if(stream == stdin) vfs_close(stdin);
	} else {
		while(!kb_status());
		c = kb_getkey();
	}
	return c;
}

char *gets(char *str) {
	uint32_t i = 0, last_bytes = 0;
	while(1) {
		uint32_t c = getc(stdin);
		if(c >= KB_NSKC_START) continue; // nonstandard stuff, bypass
		putchar(c);
		if(c == '\n' || c == '\r') {
			break;
		} else if(c == '\b') {
			putchar(' ');
			if(i > 0) {
				putchar('\b');
				memset((void*) &str[i - last_bytes], 0, last_bytes);
				i -= last_bytes;
			}
		} else {
			last_bytes = utf8_translate_8(c, (uint8_t*) &str[i]);
			i += last_bytes;
		}
	}
	str[i] = '\0';
	return str;
}
