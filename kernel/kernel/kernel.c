/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <textmode.h>
#include <timer.h>
#include <pfa.h>
#include <paging.h>
#include <assert.h>
#include <rtc.h>
#include <framebuffer.h>
#include <keyboard.h>
#include <mouse.h>
#include <string.h>
#include <vfs.h>
#include <beep.h>
#include <bpb.h>
#include <fat12.h>
#include <psf.h>
#include <task.h>
#include <tty.h>
#include <devfs.h>
#include <elf32.h>
#include <path.h>
#include <uart.h>
#include <mmap.h>

void loadfont(void) {
	vfs_node_t *dir = vfs_finddir(vfs_root, "boot");
	if(dir != NULL) {
		dir = vfs_finddir(dir, "fonts");
		if(dir != NULL) {
			char sel0[100] = "Don't load any fonts (use default 8x8 font)";
			uint32_t selcnt = 1;
			char **sel = malloc(sizeof(char*) * selcnt);
			sel[0] = sel0;
			for(uint32_t i = 0; ; i++) {
				struct dirent *de = vfs_readdir(dir, i);
				if(de == NULL) break;
				selcnt++;
				sel = realloc(sel, sizeof(char*) * selcnt);
				sel[i + 1] = de->name;
			}
			uint32_t disp_start = 0, pos = 0;
			text_clear();
			printf("Select PSF file to load:\nPress UP/DOWN to choose the font, then press ENTER/RETURN to select.\n\n");
			uint32_t start_y; text_getxy(NULL, &start_y);
			uint32_t disp_max = text_get_yres() - 1 - start_y;
			while(1) {
				for(uint32_t i = disp_start; i < (disp_start + disp_max) && i < selcnt; i++)
					printf("%c %s\n", (pos == i) ? '>' : ' ', sel[i]);
				uint32_t c = getc(NULL);
				if(c == KB_ARROW_UP && pos > 0) pos--;
				else if(c == KB_ARROW_DOWN && pos < selcnt - 1) pos++;
				else if(c == '\r') break;
				if(pos == disp_start && disp_start > 0) disp_start--;
				else if(pos == disp_start + disp_max) disp_start++;
				for(uint32_t i = disp_start; i < (disp_start + disp_max) && i < selcnt; i++) {
					text_setxy(0, start_y + i - disp_start); text_clrline();
				}
				text_setxy(0, start_y);
			}
			if(pos) {
				vfs_node_t *file = vfs_finddir(dir, sel[pos]);
				if(file != NULL) {
					vfs_open(file, 0);
					void *fontdata = malloc(file->len);
					vfs_read(file, 0, file->len, (uint8_t*) fontdata);
					vfs_close(file);
					psf_init(fontdata, file->len);
					free(fontdata);
				}
			}
			for(uint32_t i = 1; i < selcnt; i++) free(sel[i]);
			free(sel);
		}
	}
	text_clear();
}

/* recursive function to list all files and directories */
void listdir(vfs_node_t *dir, uint8_t tabs) {
	for(uint32_t i = 0; ; i++) {
		struct dirent *de = vfs_readdir(dir, i);
		if(de == NULL) break;
		vfs_node_t *node = vfs_finddir(dir, de->name);
		// printf("de %x node %x\n", de, node);
		if(node == NULL) goto cont;
		printf("0x%8x: ", de->inode);
		for(uint8_t j = 0; j < tabs; j++) putchar(' ');
		printf("%s", de->name);
		if(node->flags == VFS_TYPE_DIR) {
			printf("/\n");
			listdir(node, tabs + 1);
		}
		else printf("\n");
cont:
		free(de);
		continue;
	}
}

void test_task() {
	char buf[128] = {0};
	char buf_n[128] = {0};
	int32_t sx = 0;
	while(1) {
		char *weekdays[] = {NULL, "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
		rtc_t time;
		rtc_gettime(&time);
		sprintf(buf_n, "%s %2u-%2u-%4u %2u:%2u:%2u", weekdays[time.day], time.date, time.month, time.year, time.hour, time.min, time.sec);
		if(strcmp(buf, buf_n) != 0) {
			text_setxy(sx, 0); for(uint32_t i = 0; i < strlen(buf); i++) putchar(' ');
			strcpy(buf, buf_n);
		}
		sx = text_get_xres() - strlen(buf) - 1; if(sx < 0) sx = 0;
		text_setbg(TEXT_GREEN); text_setfg(TEXT_WHITE);
		text_setxy(sx, 0); printf("%s", buf);
	}
}

extern void kainit_stage2(void);

void kmain(void *initrd, uintptr_t initrd_size) {
	pfa_init(mmap_getsize());
	kb_clear();
	paging_init();
	heap_init(0xF0000000, 4194304, 0x10000000); // initialize heap with 4096 bytes of initial memory
	current_dir = paging_clone(kernel_dir);
	paging_switch(current_dir);
	task_init();
	uart_init(0, 115200, 8, 1, UART_PARITY_NONE);
	text_clear();
	kainit_stage2();
	printf("Fusion - Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).\n");
	printf("This program comes with ABSOLUTELY NO WARRANTY.\n");
	printf("This is free software, and you are welcome to redistribute it under certain conditions.\n\n");
	printf("Paging structure ID: 0x%x\n", current_dir);
	(void) initrd_size;
	fat12_vfs_init(initrd);
	devfs_init();
	tty_vfs_init();
	stdio_vfs_init();
	fb_vfs_init();
	for(uint8_t y = 0; y < 16; y++) {
		for(uint8_t x = 0; x < 16; x++) {
			text_setbg(y); text_setfg(x);
			printf("%X%X", y, x);
		}
		putchar('\n');
	}
	/* load kernel symbols */
	vfs_node_t *ksym = vfs_finddir(vfs_root, "kernel.sym");
	if(ksym != NULL) {
		vfs_open(ksym, 0);
		uint8_t *t = malloc(ksym->len);
		vfs_read(ksym, 0, ksym->len, t);
		elf32_load((void*) t, ksym->len, ELF32_MODE_EXPORT_SYMS, NULL);
		free(t);
		vfs_close(ksym);
	}
	text_setbg(TEXT_BLACK); text_setfg(TEXT_WHITE);
	loadfont();
	text_setfg(TEXT_LGREEN);
	printf("Root directory listing:\n");
	listdir(vfs_root, 0);
	putchar('\n');
	text_setfg(TEXT_WHITE);
	task_create(test_task, 0, current_dir);

	/* load fsh */
	vfs_node_t *file = path_getnode("/bin/fsh.elf");
	if(file == NULL) {
		puts("ERROR: fsh not found\n");
		while(1);
	}
	vfs_open(file, 0);
	uint8_t *t = malloc(file->len);
	vfs_read(file, 0, file->len, t);
	uintptr_t addr = 0;
	void (*ep)() = (void*) elf32_load((void*) t, file->len, ELF32_MODE_LOAD_BINS, &addr);
	vfs_close(file);
	free(t);
	(*ep)();
	free((void*) addr);
	while(1);
}
