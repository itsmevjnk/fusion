/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
#ifndef PCM_H
#define PCM_H

#include <stddef.h>
#include <stdint.h>

/* PCM frame structure - this abstraction layer uses the sample-frame scheme */
typedef struct {
	int16_t left;
	int16_t right;
} __attribute__ ((packed)) pcm_frame_t;

/* PCM device structure */
typedef uint32_t (*pcm_load_t)(struct pcm_device*, pcm_frame_t*, uint32_t); // load frames to buffer for playing
typedef void (*pcm_play_t)(struct pcm_device*); // start playing the loaded frame
typedef void (*pcm_stop_t)(struct pcm_device*); // stop playing the frame (or actually pause, since it can be resumed)
typedef struct pcm_device {
	char name[128]; // device name
	uint8_t ch_out; // channels supported for playback (max 2)
	uint32_t max_frames; // maximum number of frames allowed (e.g. can be stored in DMA buffer)
	uint32_t impl; // implementation-defined number, probably used when multiple devices using the same driver are plugged in
	void (*play_cb)(); // play callback function, called when there's no more frames left to play (e.g. load next frames)
	pcm_load_t load;
	pcm_play_t play;
	pcm_stop_t stop;
} __attribute__ ((packed)) pcm_device_t;

uint32_t pcm_load(pcm_device_t *device, pcm_frame_t *frame, uint32_t n);
void pcm_play(pcm_device_t *device);
void pcm_stop(pcm_device_t *device);

#endif