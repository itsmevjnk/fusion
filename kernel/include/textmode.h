/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEXTMODE_H
#define TEXTMODE_H

#include <stddef.h>
#include <stdint.h>
#include <framebuffer.h>

#define TEXT_BLACK					0
#define TEXT_BLUE					1
#define TEXT_GREEN					2
#define TEXT_CYAN					3
#define TEXT_RED					4
#define TEXT_MAGENTA				5
#define TEXT_BROWN					6
#define TEXT_LGRAY					7
#define TEXT_DGRAY					8
#define TEXT_LBLUE					9
#define TEXT_LGREEN					10
#define TEXT_LCYAN					11
#define TEXT_LRED					12
#define TEXT_LMAGENTA				13
#define TEXT_YELLOW					14
#define TEXT_WHITE					15

extern fb_device_t *text_fb;

void text_writec(uint32_t c); // print a character without moving the cursor
void text_putc(uint32_t c); // print a character
void text_puts(const char *s); // print a string
void text_scrollup(uint32_t lines, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1); // scroll screen up, (x0, y0) is the coordinates of the upper left corner, (x1, y1) is the coordinates of the lower right corner
void text_scrolldown(uint32_t lines, uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1); // scroll screen down, (x0, y0) is the coordinates of the upper left corner, (x1, y1) is the coordinates of the lower right corner
void text_setbg(uint8_t bg); // set background color
void text_setfg(uint8_t fg); // set foreground color
uint8_t text_getbg(void); // get current background color
uint8_t text_getfg(void); // get current foreground color
void text_cursor_on(uint8_t begin, uint8_t end); // turn on cursor
void text_cursor_off(void); // turn off cursor
void text_setxy(uint32_t x, uint32_t y); // set current coordinates
void text_getxy(uint32_t *x, uint32_t *y); // get current coordinates
void text_clear(void); // clear screen
uint32_t text_get_xres(void); // get x resolution
uint32_t text_get_yres(void); // get y resolution
void text_clrline(void); // clear current line
uint32_t text_data_size(void); // get size of text data structure (for allocating)
void text_data_load(void *src); // load text data
void text_data_save(void *dst); // save text data

#endif
