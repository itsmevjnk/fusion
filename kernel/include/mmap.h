/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MMAP_H
#define MMAP_H

#include <stddef.h>
#include <stdint.h>

#define MMAP_AVAILABLE					0
#define MMAP_RESERVED					1
#define MMAP_UNDEF						0xFF

typedef struct {
	uintptr_t start, len;
	uint8_t flags;
} __attribute__ ((packed)) mmap_entry_t;

extern mmap_entry_t *mmap;
extern uint32_t mmap_entries;

void mmap_register(uintptr_t start, uintptr_t len, uint8_t flags);
uint8_t mmap_status(uintptr_t addr);
uintptr_t mmap_getsize(void);

#endif
