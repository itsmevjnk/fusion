/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
#ifndef I386_PC_DMA_H
#define I386_PC_DMA_H

#include <stddef.h>
#include <stdint.h>

/* DMA0 ports */
#define DMA0_REG_ADDR_0					0x00
#define DMA0_REG_CNT_0					0x01
#define DMA0_REG_ADDR_1					0x02
#define DMA0_REG_CNT_1					0x03
#define DMA0_REG_ADDR_2					0x04
#define DMA0_REG_CNT_2					0x05
#define DMA0_REG_ADDR_3					0x06
#define DMA0_REG_CNT_3					0x07
#define DMA0_REG_STATUS					0x08
#define DMA0_REG_COMMAND				0x08
#define DMA0_REG_REQUEST				0x09
#define DMA0_REG_CHMASK					0x0A
#define DMA0_REG_MODE					0x0B
#define DMA0_REG_CLRFF					0x0C
#define DMA0_REG_TEMP					0x0D
#define DMA0_REG_MCLR					0x0D
#define DMA0_REG_CLRMASK				0x0E
#define DMA0_REG_MASK					0x0F

/* DMA1 ports */
#define DMA1_REG_ADDR_4					0xC0
#define DMA1_REG_CNT_4					0xC2
#define DMA1_REG_ADDR_5					0xC4
#define DMA1_REG_CNT_5					0xC6
#define DMA1_REG_ADDR_6					0xC8
#define DMA1_REG_CNT_6					0xCA
#define DMA1_REG_ADDR_7					0xCC
#define DMA1_REG_CNT_7					0xCE
#define DMA1_REG_STATUS					0xD0
#define DMA1_REG_COMMAND				0xD0
#define DMA1_REG_REQUEST				0xD2
#define DMA1_REG_CHMASK					0xD4
#define DMA1_REG_MODE					0xD6
#define DMA1_REG_CLRFF					0xD8
#define DMA1_REG_TEMP					0xDA
#define DMA1_REG_CLRMASK				0xDC
#define DMA1_REG_MASK					0xDE

/* DMA extended page address registers */
#define DMA_REG_PAGE_2					0x81
#define DMA_REG_PAGE_3					0x82
#define DMA_REG_PAGE_1					0x83
#define DMA_REG_PAGE_6					0x87
#define DMA_REG_PAGE_7					0x88
#define DMA_REG_PAGE_5					0x89

/* bitmask for command registers */
#define DMA_CMD_MMT						(1 << 0) // memory to memory transfer
#define DMA_CMD_ADHE					(1 << 0) // channel 0 address hold
#define DMA_CMD_COND					(1 << 0) // controller enable
#define DMA_CMD_COMP					(1 << 0) // timing
#define DMA_CMD_PRIO					(1 << 0) // priority
#define DMA_CMD_EXTW					(1 << 0) // write selection
#define DMA_CMD_DROP					(1 << 0) // DMA request
#define DMA_CMD_DACKP					(1 << 0) // DMA acknowledge

/* bitmask for mode registers */
#define DMA_MODE_CH0					0
#define DMA_MODE_CH1					(1 << 0)
#define DMA_MODE_CH2					(1 << 1)
#define DMA_MODE_CH3					(1 << 0) | (1 << 1)
#define DMA_MODE_WRITE					(1 << 2)
#define DMA_MODE_READ					(1 << 3)
#define DMA_MODE_REINIT					(1 << 4)
#define DMA_MODE_IDEC					(1 << 5)
#define DMA_MODE_ONDEMAND				0
#define DMA_MODE_SINGLE					(1 << 6)
#define DMA_MODE_BLK					(1 << 7)
#define DMA_MODE_CASCADE				(1 << 6) | (1 << 7)

void dma_set_addr(uint8_t channel, uint16_t addr);
void dma_set_cnt(uint8_t channel, uint16_t cnt);
void dma_set_page(uint8_t channel, uint8_t page);
void dma_mask(uint8_t channel);
void dma_unmask(uint8_t channel);
void dma_set_mode(uint8_t channel, uint8_t mode);
void dma_reset_ff(uint8_t dma);
void dma_reset(uint8_t dma);
void dma_unmask_all(uint8_t dma);

#endif