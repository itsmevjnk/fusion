/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef I386_PC_PS2_H
#define I386_PC_PS2_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
	uint8_t exist, connected;
	uint16_t id;
	uint8_t scset;
	/* IRQ stuff */
	volatile uint8_t bufptr;
	volatile uint8_t buf[16];
	uint8_t process; // set to 1 if the received byte would go straight to ps2_process()
	/* keyboard state machine */
	uint8_t altlut; // set to 1 to use alternate scancode lookup table (aka 0xE0)
	uint8_t brk; // set to 1 if it's a break code
	uint8_t special; // 0 = normal case, 1 = print screen, 2 = pause
	uint8_t skip_rem; // remaining codes to skip, used when dealing with pause
	/* mouse state machine */
	uint8_t mousebuf[3];
	uint8_t mousebuf_ptr;
} ps2_port_t;
extern ps2_port_t ps2_ports[2];

uint8_t ps2_read_status(void);
uint8_t ps2_read_data(void);
void ps2_write_data(uint8_t data);
void ps2_write_command(uint8_t cmd);
void ps2_disable_port(uint8_t port);
void ps2_enable_port(uint8_t port);
uint8_t ps2_get_ccb(void);
void ps2_set_ccb(uint8_t ccb);
uint8_t ps2_interface_test(uint8_t port);
void ps2_device_send_data(uint8_t device, uint8_t data);
void ps2_device_set_scanning(uint8_t device, uint8_t state);
uint8_t ps2_device_get_scset(uint8_t device);
int8_t ps2_device_reset(uint8_t device);
int8_t ps2_init(void);
void ps2_reboot(void);

#endif
