/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <stddef.h>
#include <stdint.h>

#define KEYCODE(x, y)					((y << 5) | x)

extern volatile uint8_t kb_stattab[256]; // key status

extern volatile uint16_t kb_modstat; // modifier keys status
/* modifier keys status bits:
 * 0 - num lock (latching)
 * 1 - caps lock (latching)
 * 2 - scroll lock (latching)
 * 3 - left ctrl (momentary)
 * 4 - right ctrl (momentary)
 * 5 - left alt (momentary)
 * 6 - right alt (AltGr) (momentary)
 * 7 - left shift (momentary)
 * 8 - right shift (momentary)
 * 9 - either ctrl (momentary)
 * 10 - either alt (momentary)
 * 11 - either shift (momentary)
 */

#define KB_BUFSIZE			16

extern uint32_t kb_buffer[KB_BUFSIZE]; // key buffer
typedef struct {
	uint32_t from; // original
	uint32_t to; // modified
} __attribute__ ((packed)) kb_modifier_ent_t;

typedef struct {
	uint16_t bitmask; // keyboard modifier key status bitmask
	uint16_t mask_res; // desired result of keyboard status AND bitmask
	uint16_t ents; // the amount of entries in the below list
	kb_modifier_ent_t entries[256]; // a list of what a character will be modified into
} __attribute__ ((packed)) kb_modifier_t;

typedef struct {
	uint8_t *name; // layout name
	uint32_t default_map[256]; // default layout (aka what the layout is without modifier keys)
	uint16_t mods_cnt; // the number of modifiers
	kb_modifier_t *mods[256]; // keyboard modifiers
} __attribute__ ((packed)) kb_layout_t;

extern kb_layout_t *kb_curr_layout; // current layout

/* custom keycodes */
#define KB_NSKC_START					0xFFFFFF00 // start of non-standard keycodes
enum keycodes {
	KB_ESC = 0x1B, // escape key
	KB_DEL = 0x7F, // delete key
	/* function keys: F1 - F12 */
	KB_F1 = KB_NSKC_START,
	KB_F2,
	KB_F3,
	KB_F4,
	KB_F5,
	KB_F6,
	KB_F7,
	KB_F8,
	KB_F9,
	KB_F10,
	KB_F11,
	KB_F12,
	/* modifier keys */
	KB_NUM_LOCK,
	KB_CAPS_LOCK,
	KB_SCROLL_LOCK,
	KB_CTRL_L,
	KB_CTRL_R,
	KB_ALT_L,
	KB_ALT_R, // AltGr,
	KB_SHIFT_L,
	KB_SHIFT_R,
	/* arrow keys */
	KB_ARROW_UP,
	KB_ARROW_DOWN,
	KB_ARROW_LEFT,
	KB_ARROW_RIGHT,
	/* keypad keys */
	KB_KPAD_SLASH,
	KB_KPAD_STAR,
	KB_KPAD_DASH,
	KB_KPAD_PLUS,
	KB_KPAD_DOT, // acts as Delete when NumLock is off
	KB_KPAD_0, // acts as Insert when NumLock is off
	KB_KPAD_1, // acts as End when NumLock is off
	KB_KPAD_2, // acts as Down when NumLock is off
	KB_KPAD_3, // acts as Page Down when NumLock is off
	KB_KPAD_4, // acts as Left when NumLock is off
	KB_KPAD_5,
	KB_KPAD_6, // acts as Right when NumLock is off
	KB_KPAD_7, // acts as Home when NumLock is off
	KB_KPAD_8, // acts as Up when NumLock is off
	KB_KPAD_9, // acts as Page Up when NumLock is off
	/* other keys on standard keyboards */
	KB_PRTSC, // Print Screen/SysRq
	KB_PAUSE, // Pause/Break
	KB_INSERT,
	KB_HOME,
	KB_END,
	KB_PAGE_UP,
	KB_PAGE_DOWN,
	KB_MENU,
	/* Windows keys */
	KB_WIN_L,
	KB_WIN_R,
	/* ACPI keys */
	KB_ACPI_POWER,
	KB_ACPI_SLEEP,
	KB_ACPI_WAKE,
	/* multimedia keys */
	KB_MM_PLAY, // play/pause
	KB_MM_STOP,
	KB_MM_PREV, // previous track
	KB_MM_NEXT, // next track
	KB_MM_VUP, // volume up
	KB_MM_VDOWN, // volume down
	KB_MM_MUTE,
	KB_MM_COMP, // My Computer
	KB_MM_MAIL,
	KB_MM_CALC, // calculator
	KB_MM_MSEL, // media select
	/* Web keys */
	KB_WWW_BACK,
	KB_WWW_NEXT,
	KB_WWW_REFRESH,
	KB_WWW_STOP,
	KB_WWW_SEARCH,
	KB_WWW_FAV,
	KB_WWW_HOME,
};

void kb_register(uint8_t keycode, uint8_t state); // register a key press/release event
uint8_t kb_status(void); // returns 1 if keyboard buffer is not empty
uint32_t kb_getkey(void); // get the last key on the keyboard buffer
void kb_clear(void); // clear the key buffer
void kb_postreg(void); // to be called at the end of the keyboard data processing function

#endif
