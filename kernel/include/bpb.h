/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BPB_H
#define BPB_H

#include <stddef.h>
#include <stdint.h>

/* BIOS Parameter Block (BPB) */
typedef struct {
	uint8_t jmp[3]; // jump instruction, typically EB 3C 90 or EB FE 90
	uint8_t oemid[8]; // OEM identifier, MSWIN4.1 recommended by Microsoft
	uint16_t bpsect; // bytes per sector
	uint8_t spclus; // sectors per cluster
	uint16_t rsect; // number of reserved sectors (including sector 0)
	uint8_t fat; // number of FATs
	uint16_t rdent; // number of root directory entries
	uint16_t sect; // number of sectors (set to 0 if there are more than 65535 sectors)
	uint8_t mdesc; // media descriptor type
	uint16_t spfat; // number of sectors per FAT (FAT12/16 only)
	uint16_t sptrk; // number of sectors per track (only useful for CHS addressing)
	uint16_t heads; // number of heads/sides (only useful for CHS addressing)
	uint32_t hsect; // number of hidden sectors (the LBA of the beginning of the partition)
	uint32_t lsect; // large sector count (set if there are more than 65535 sectors)
	uint8_t extra[474]; // extra information (EBPB/boot code/both)
	uint16_t sig; // boot signature (0xAA55)
} __attribute__ ((packed)) bpb_t;

#endif
