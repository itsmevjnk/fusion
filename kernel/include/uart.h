/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UART_H
#define UART_H

#include <stddef.h>
#include <stdint.h>

#define UART_PARITY_NONE              0
#define UART_PARITY_ODD               1
#define UART_PARITY_EVEN              2
#define UART_PARITY_MARK              3
#define UART_PARITY_SPACE             4

void uart_init(uint8_t port, uint32_t baud, uint8_t dbit, uint8_t sbit, uint8_t parity);
void uart_putc(uint8_t port, uint8_t c);
void uart_puts(uint8_t port, const char *s);
uint8_t uart_getc(uint8_t port);

#endif
