/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IO_H
#define IO_H

#include <stddef.h>
#include <stdint.h>

void outb(uintptr_t port, uint8_t val);
uint8_t inb(uintptr_t port);
void io_wait(void);
uint16_t inw(uintptr_t port);
uint32_t inl(uintptr_t port);
void outw(uintptr_t port, uint16_t val);
void outl(uintptr_t port, uint32_t val);

#endif
