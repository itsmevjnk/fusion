/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TIMER_H
#define TIMER_H

#include <stddef.h>
#include <stdint.h>

extern volatile uint64_t timer_ticks; // number of microseconds passed since the timer is initialized

typedef struct {
	uint8_t active; // set to 1 if event is active
	uint8_t mode; // 0 = one-shot (no autoreset), 1 = periodic (autoreset)
	void (*func)(); // pointer to function to call when time's up
	uint64_t begin_tick; // when this event was started
	uint64_t ticks; // how many ticks
} timer_event_t;
extern timer_event_t timer_events[1024];

#define TIMER_ONESHOT				0
#define TIMER_PERIODIC				1

void usleep(uint64_t usec);
void sleep(uint32_t seconds);
void ustall(uint64_t usec);
void stall(uint32_t seconds);
void timer_add_ticks(uint64_t ticks);
uint16_t timer_add_event(uint8_t mode, void (*func)(), uint64_t ticks);
void timer_remove_event(uint16_t event);

#endif
