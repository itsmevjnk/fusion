/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FAT12_H
#define FAT12_H

#include <stddef.h>
#include <stdint.h>

/* Extended BIOS Parameter Block (EBPB) for FAT12/16 */
typedef struct {
	uint8_t drvno; // drive number (only useful if we're using INT 13h)
	uint8_t reserved; // reserved (used in Windows NT to store flags)
	uint8_t sig; // signature (0x28/29)
	uint32_t sn; // volume ID/serial number
	uint8_t label[11]; // volume label
	uint8_t sysid[8]; // system identifier string (ignored)
	uint8_t code[448]; // boot code
} __attribute__ ((packed)) fat12_ebpb_t;

/* FAT date structure */
typedef struct {
	uint8_t day : 5;
	uint16_t month : 4;
	uint8_t year : 7;
} __attribute__ ((packed)) fat12_date_t;

/* FAT time structure */
typedef struct {
	uint8_t sec : 5; // NOTE: 2-second resolution
	uint16_t min : 6;
	uint8_t hour : 5;
} __attribute__ ((packed)) fat12_time_t;

/* FAT directory entry attributes */
#define FATTR_RO				0x01		// read only
#define FATTR_HIDDEN		0x02		// hidden
#define FATTR_SYSTEM		0x04		// system
#define FATTR_VOLID			0x08		// volume ID
#define FATTR_DIR				0x10		// directory
#define FATTR_ARCH			0x20		// archive
#define FATTR_LFN				0x0F		// long filename entry

/* FAT directory entry */
typedef struct {
	uint8_t name[11]; // 8.3 file name
	uint8_t attribs; // file/folder attributes
	uint8_t reserved; // reserved for use in Windows NT
	uint8_t c10sec; // creation time second (100ms resolution)
	fat12_time_t ctime; // creation time
	fat12_date_t cdate; // creation date
	fat12_date_t adate; // last accessed date
	uint16_t hclus; // high 16 bits of first cluster number (unused in FAT12/16)
	fat12_time_t mtime; // last modification time
	fat12_date_t mdate; // last modification date
	uint16_t lclus; // low 16 bits of first cluster number
	uint32_t size; // file size in bytes
} __attribute__ ((packed)) fat12_dirent_t;

/* long file name entry */
typedef struct {
	uint8_t order; // order in the long file name sequence
	uint16_t name_1[5]; // first 5 characters of the entry
	uint8_t attribs; // attributes (always 0x0F)
	uint8_t type; // long entry type (0 for name entries)
	uint8_t checksum; // short filename checksum
	uint16_t name_2[6]; // next 6 characters of the entry
	uint16_t zero; // always 0
	uint16_t name_3[2]; // last 2 characters
} __attribute__ ((packed)) fat12_lfnent_t;

void fat12_vfs_init(void *initrd);

#endif
