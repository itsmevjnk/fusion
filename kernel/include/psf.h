/*
 * Copyright (C) 2020 Succboy6000 (Thanh Vinh Nguyen).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PSF_H
#define PSF_H

#include <stddef.h>
#include <stdint.h>

/* PSF1 header */
#define PSF1_MAGIC				0x0436
#define PSF1_MODE_512			(1 << 0)
#define PSF1_MODE_TAB			(1 << 1)
#define PSF1_MODE_SEQ			(1 << 2)
#define PSF1_MODE_MAX			(PSF_MODE_512 | PSF_MODE_SEQ)
typedef struct {
	uint16_t magic;
	uint8_t mode;
	uint8_t height;
} __attribute__ ((packed)) psf1_t;

/* PSF2 header */
#define PSF2_MAGIC				0x864ab572
#define PSF2_FLAGS_UCTAB	(1 << 0)
typedef struct {
	uint32_t magic;
	uint32_t version;
	uint32_t hdr_size;
	uint32_t flags;
	uint32_t glyphs;
	uint32_t gsize;
	uint32_t height, width;
} __attribute__ ((packed)) psf2_t;

extern uint8_t *psf_glyphs; // glyphs. if this is not NULL, it means that PSF has been initialized
extern uint32_t psf_width, psf_height;

void psf_init(void* file, uint32_t size);
void psf_writec(uint32_t c, uint32_t x, uint32_t y, uint32_t bg, uint32_t fg);

#endif
